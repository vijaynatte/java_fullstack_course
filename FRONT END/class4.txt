Order list in HTML
===================
A <ol> tag is used to declare order list in HTML.

An orderlist contains numerics and alphabets.

Every orderlist contains list of items.

We can represent list of items by using <li> tag.


ex:1
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		Courses:
		<ol>
			<li>ReactJS</li>
			<li>AngularJS</li>
			<li>VueJS</li>
			<li>ExpressJS</li>
		</ol>
	</body>
</html>

ex:2
----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		Courses:
		<ol  start="101">
			<li>ReactJS</li>
			<li>AngularJS</li>
			<li>VueJS</li>
			<li>ExpressJS</li>
		</ol>
	</body>
</html>

ex:3
------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		Courses:
		<ol  type="a">
			<li>ReactJS</li>
			<li>AngularJS</li>
			<li>VueJS</li>
			<li>ExpressJS</li>
		</ol>
	</body>
</html>

ex:4
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		Courses:
		<ol  type="A">
			<li>ReactJS</li>
			<li>AngularJS</li>
			<li>VueJS</li>
			<li>ExpressJS</li>
		</ol>
	</body>
</html>


ex:5
---
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		Courses:
		<ol  type="i">
			<li>ReactJS</li>
			<li>AngularJS</li>
			<li>VueJS</li>
			<li>ExpressJS</li>
		</ol>
	</body>
</html>

ex:6
------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		Courses:
		<ol  type="I">
			<li>ReactJS</li>
			<li>AngularJS</li>
			<li>VueJS</li>
			<li>ExpressJS</li>
		</ol>
	</body>
</html>


Unorder list in HTML
===================
A <ul> tag is used to declare unorder list in HTML.

An unorderlist contains bullets.

Every unorderlist contains list of items.

We can represent list of items by using <li> tag.


ex:1
----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		Courses:
		<ul>
			<li>ReactJS</li>
			<li>AngularJS</li>
			<li>VueJS</li>
			<li>ExpressJS</li>
		</ul>
	</body>
</html>

ex:2
----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		Courses:
		<ul type="disc">
			<li>ReactJS</li>
			<li>AngularJS</li>
			<li>VueJS</li>
			<li>ExpressJS</li>
		</ul>
	</body>
</html>

ex:3
----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		Courses:
		<ul type="square">
			<li>ReactJS</li>
			<li>AngularJS</li>
			<li>VueJS</li>
			<li>ExpressJS</li>
		</ul>
	</body>
</html>

ex:4
----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		Courses:
		<ul type="circle">
			<li>ReactJS</li>
			<li>AngularJS</li>
			<li>VueJS</li>
			<li>ExpressJS</li>
		</ul>
	</body>
</html>


HTML table
=============
A table is used to represent the data.

A table is a collection of rows and columns.

A <table> tag is used to declare a table in HTML.

A <tr> tag is used to declare a table row.

A <th> tag is used to declare table heading.

A <td> tag is used to declare table data.

All table headings are bold and centered.

All table datas are left aligned and normal.


ex:1
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		<table>
			<tr>
				<th>SNO</th>
				<th>SNAME</th>
				<th>SADD</th>
			</tr>
			<tr>
				<td>101</td>
				<td>Alan</td>
				<td>Florida</td>
			</tr>
			<tr>
				<td>102</td>
				<td>Jessi</td>
				<td>Texas</td>
			</tr>
			<tr>
				<td>103</td>
				<td>Nelson</td>
				<td>Vegas</td>
			</tr>
		</table>
	</body>
</html>

Note:
----
The default border for a table is '0'.

ex:2
------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		<table border="0">
			<tr>
				<th>SNO</th>
				<th>SNAME</th>
				<th>SADD</th>
			</tr>
			<tr>
				<td>101</td>
				<td>Alan</td>
				<td>Florida</td>
			</tr>
			<tr>
				<td>102</td>
				<td>Jessi</td>
				<td>Texas</td>
			</tr>
			<tr>
				<td>103</td>
				<td>Nelson</td>
				<td>Vegas</td>
			</tr>
		</table>
	</body>
</html>


ex:3
----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		<table border="1">
			<tr>
				<th>SNO</th>
				<th>SNAME</th>
				<th>SADD</th>
			</tr>
			<tr>
				<td>101</td>
				<td>Alan</td>
				<td>Florida</td>
			</tr>
			<tr>
				<td>102</td>
				<td>Jessi</td>
				<td>Texas</td>
			</tr>
			<tr>
				<td>103</td>
				<td>Nelson</td>
				<td>Vegas</td>
			</tr>
		</table>
	</body>
</html>

ex:4
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		<table border="1" cellspacing="10px">
			<tr>
				<th>SNO</th>
				<th>SNAME</th>
				<th>SADD</th>
			</tr>
			<tr>
				<td>101</td>
				<td>Alan</td>
				<td>Florida</td>
			</tr>
			<tr>
				<td>102</td>
				<td>Jessi</td>
				<td>Texas</td>
			</tr>
			<tr>
				<td>103</td>
				<td>Nelson</td>
				<td>Vegas</td>
			</tr>
		</table>
	</body>
</html>

ex:5
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		<table border="1" cellspacing="10px" cellpadding="10px">
			<tr>
				<th>SNO</th>
				<th>SNAME</th>
				<th>SADD</th>
			</tr>
			<tr>
				<td>101</td>
				<td>Alan</td>
				<td>Florida</td>
			</tr>
			<tr>
				<td>102</td>
				<td>Jessi</td>
				<td>Texas</td>
			</tr>
			<tr>
				<td>103</td>
				<td>Nelson</td>
				<td>Vegas</td>
			</tr>
		</table>
	</body>
</html>

ex:6
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		<table border="1" 
				cellspacing="10px" 
					cellpadding="10px"
						align="right">
			<tr>
				<th>SNO</th>
				<th>SNAME</th>
				<th>SADD</th>
			</tr>
			<tr>
				<td>101</td>
				<td>Alan</td>
				<td>Florida</td>
			</tr>
			<tr>
				<td>102</td>
				<td>Jessi</td>
				<td>Texas</td>
			</tr>
			<tr>
				<td>103</td>
				<td>Nelson</td>
				<td>Vegas</td>
			</tr>
		</table>
	</body>
</html>

ex:7
----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		<table border="1" 
				cellspacing="10px" 
					cellpadding="10px"
						align="center">
			<tr>
				<th>SNO</th>
				<th>SNAME</th>
				<th>SADD</th>
			</tr>
			<tr>
				<td>101</td>
				<td>Alan</td>
				<td>Florida</td>
			</tr>
			<tr>
				<td>102</td>
				<td>Jessi</td>
				<td>Texas</td>
			</tr>
			<tr>
				<td>103</td>
				<td>Nelson</td>
				<td>Vegas</td>
			</tr>
		</table>
	</body>
</html>

ex:8
------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		<table border="1" 
				cellspacing="10px" 
					cellpadding="10px"
						align="left">
			<tr>
				<th>SNO</th>
				<th>SNAME</th>
				<th>SADD</th>
			</tr>
			<tr>
				<td>101</td>
				<td>Alan</td>
				<td>Florida</td>
			</tr>
			<tr>
				<td>102</td>
				<td>Jessi</td>
				<td>Texas</td>
			</tr>
			<tr>
				<td>103</td>
				<td>Nelson</td>
				<td>Vegas</td>
			</tr>
		</table>
	</body>
</html>

ex:9
------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		<table border="1" 
				cellspacing="10px" 
					cellpadding="10px"
						align="center"
							width="100%">
			<tr>
				<th>SNO</th>
				<th>SNAME</th>
				<th>SADD</th>
			</tr>
			<tr>
				<td>101</td>
				<td>Alan</td>
				<td>Florida</td>
			</tr>
			<tr>
				<td>102</td>
				<td>Jessi</td>
				<td>Texas</td>
			</tr>
			<tr>
				<td>103</td>
				<td>Nelson</td>
				<td>Vegas</td>
			</tr>
		</table>
	</body>
</html>

ex:10
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		<table border="1" 
				cellspacing="10px" 
					cellpadding="10px"
						align="center"
							width="100%">
			<tr bgcolor="cyan">
				<th>SNO</th>
				<th>SNAME</th>
				<th>SADD</th>
			</tr>
			<tr>
				<td>101</td>
				<td>Alan</td>
				<td>Florida</td>
			</tr>
			<tr>
				<td>102</td>
				<td>Jessi</td>
				<td>Texas</td>
			</tr>
			<tr>
				<td>103</td>
				<td>Nelson</td>
				<td>Vegas</td>
			</tr>
		</table>
	</body>
</html>

ex:11
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		<table border="1" 
				cellspacing="10px" 
					cellpadding="10px"
						align="center"
							width="100%"
							  bgcolor="#dfe6e9">
			<tr bgcolor="cyan">
				<th>SNO</th>
				<th>SNAME</th>
				<th>SADD</th>
			</tr>
			<tr>
				<td>101</td>
				<td>Alan</td>
				<td>Florida</td>
			</tr>
			<tr>
				<td>102</td>
				<td>Jessi</td>
				<td>Texas</td>
			</tr>
			<tr>
				<td>103</td>
				<td>Nelson</td>
				<td>Vegas</td>
			</tr>
		</table>
	</body>
</html>

ex:12
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		<table border="1" 
				cellspacing="10px" 
					cellpadding="10px"
						align="center"
							width="100%"
							  bgcolor="#dfe6e9">

			<caption>Student Information Details</caption>
			<tr bgcolor="cyan">
				<th>SNO</th>
				<th>SNAME</th>
				<th>SADD</th>
			</tr>
			<tr>
				<td>101</td>
				<td>Alan</td>
				<td>Florida</td>
			</tr>
			<tr>
				<td>102</td>
				<td>Jessi</td>
				<td>Texas</td>
			</tr>
			<tr>
				<td>103</td>
				<td>Nelson</td>
				<td>Vegas</td>
			</tr>
		</table>
	</body>
</html>

ex:13
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
	</head>
	<body>
		<table border="1" 
				cellspacing="10px" 
					cellpadding="10px"
						align="center"
							width="100%"
							  bgcolor="#dfe6e9">

			<caption>Student Information Details</caption>
			<tr bgcolor="cyan">
				<th>SNO</th>
				<th>SNAME</th>
				<th>SADD</th>
			</tr>
			<tr>
				<td>101</td>
				<td>Alan</td>
				<td>Florida</td>
			</tr>
			<tr>
				<td>102</td>
				<td>Jessi</td>
				<td>Texas</td>
			</tr>
			<tr>
				<td>103</td>
				<td>Nelson</td>
				<td>Vegas</td>
			</tr>
			<tr>
				<td colspan="3"><center><h4>Thank You</h4></center></td>
			</tr>
		</table>
	</body>
</html>



Interview Questions
=====================

1)What is the difference between Tag, Element and Attribute?

HTML tag:
---------
	HTML tag starts with '<' and ends with '>'.
	Ex:
		<html>,<head>, <body> and etc.


HTML element:
--------------
	An HTML element is defined by a start tag, some content and end tag.
	Ex:
		<h1>This is Heading Tag</h1>

HTML attribute:
-----------------
	Attributes provide additional information about elements.
	Attributes are always specified in the start tag.
	Attributes usually come in name/value pairs.
	Ex:
		<body bgcolor="#FFFF00">


2) Types of elements in HTML ?

We have two types of elements.

1)Block Elements

2)Inline Elements  

1)Block Elements
---------------
Block element always starts with new line and it will take full width of a 
viewport or device.

We have following list of block elements in html.

ex:
	<h1>, <p> , <div> , <ol> , <ul> and etc.

ex:
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>

	</head>
	<body>
		<p style="border:2px solid black;">This is paragraph</p>
		<div style="border:2px solid red;">This is division tag</div>
		<h1 style="border:2px solid blue;">This is heading tag</h1>
	</body>
</html>


2)Inline Elements  
------------------
Inline element always starts with same line and takes width as much as required.

We have following list of inline elements.

ex:
	<b>, <i> , <u> , <span> , <strong> and etc.	

ex:
	
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>

	</head>
	<body>
		<b style="border:2px solid black;">This is bold tag</b>
		<i style="border:2px solid red;">This is italic tag</i>
		<span style="border:2px solid blue;">This is span tag</span>
	</body>
</html>

3)Types of list in HTML ?

We have three types of list in HTML.

1)Order list 

2)Unorder list 

3)Definition list 


1)Order list:
---------------
	It is used to group a set of related items in a specific order.
	Ex:
		<ol>
			<li>HTML</li>
			<li>CSS</li>
			<li>JAVASCRIPT</li>
		</ol>

2)Unorder list:
-----------------
	It is used to group a set of related items in no particular order.
	Ex:
		<ul>
			<li>HTML</li>
			<li>CSS</li>
			<li>JAVASCRIPT</li>
		</ul>


3)Description list:
---------------------	
	It is used to display name/value pairs such as terms and definitions.
	Ex:
		<dl>
			<dt>HTML</dt>
			<dd>HTML is widely used language on web.</dd>
		</dl>


















































































































































































































































































































































