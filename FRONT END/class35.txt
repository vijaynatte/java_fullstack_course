Bootstrap Cards
==================
A card in Bootstrap 5 is a bordered box with some padding around its content. It includes options for headers, footers, content, colors, etc.

A basic card is created with the .card class, and content inside the card has a .card-body class:

The .card-header class adds a heading to the card and the .card-footer class adds a footer to the card.

Use .card-title to add card titles to any heading element. 

The .card-text class is used to remove bottom margins for a <p> element if it is the last child (or the only one) inside .card-body.

Add .card-img-top  to an <img> to place the image at the top inside the card. 

Note that we have added the image outside of the .card-body to span the entire width:



ex:1
-----
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  	
  </head>
  <body>
  		
  		<div class="container mt-5">

  			<div class="card">
  				<div class="card-header">
  					Card Header
  				</div>	
  				<div class="card-body">
  					Card Body
  				</div>
  				<div class="card-footer">
  					Card Footer
  				</div>
  			</div>

  		</div>
  	
  </body>
</html>


ex:2
------
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  	
  </head>
  <body>
  		
  		<div class="container mt-5">

  			<div class="card">
  				<div class="card-header">
  					Card Header
  				</div>	
  				<div class="card-body">
  					<div class="card-title">IHUB TALENT MANAGEMENT</div>
  					<div class="card-text">Training institute</div>
  				</div>
  				<div class="card-footer">
  					Card Footer
  				</div>
  			</div>

  		</div>
  	
  </body>
</html>


ex:3
-----
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  	
  </head>
  <body style="background-color:lightgray">
  		
  		<div class="container mt-5 ">

  			<div class="row">

  				<div class="col-md-4 col-sm-6 col-12 ">
  					<div class="card">
  						<img src="images/thumbnail1.jpg">
  						<div class="card-body bg-light">
  								<div class="card-title text-center">THUMBNAIL1</div>
  								<div class="card-text text-center">Our Identity</div>
  						</div>	
  					</div>
  				</div>

  				<div class="col-md-4 col-sm-6 col-12 ">
  					<div class="card">
  						<img src="images/thumbnail2.jpg">
  						<div class="card-body bg-light">
  								<div class="card-title text-center">THUMBNAIL2</div>
  								<div class="card-text text-center">Our services</div>
  						</div>	
  					</div>
  				</div>

  				<div class="col-md-4 col-sm-6 col-12 ">
  					<div class="card">
  						<img src="images/thumbnail3.jpg">
  						<div class="card-body bg-light">
  								<div class="card-title text-center">THUMBNAIL3</div>
  								<div class="card-text text-center">Our Team</div>
  						</div>	
  					</div>
  				</div>	

  				
  			</div>

  		</div>
  	
  </body>
</html>



Bootstrap List Groups
=====================
The most basic list group is an unordered list with list items.

ex:
<ul>
  					<li>HTML</li>
  					<li>CSS</li>
  					<li>JAVASCRIPT</li>
</ul>

To create a basic list group, use an <ul> element with class .list-group,
 and <li> elements with class .list-group-item.


ex:1
-----
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  	
  </head>
  <body >
  		
  		<div class="container mt-5">
  				<ul class="list-group">
  					<li class="list-group-item">HTML</li>
  					<li class="list-group-item">CSS</li>
  					<li class="list-group-item">JavaScript</li>
  					<li class="list-group-item">Bootstrap</li>	
  				</ul>
  		</div>
  	
  </body>
</html>

we can also add badges to a list group.

To create a badge, create a <span> element with class .badge inside
the list item:

The badges will automatically be positioned on the right side.

ex:2
-------
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  	
  </head>
  <body >
  		
  		<div class="container mt-5">
  				<ul class="list-group">
  					<li class="list-group-item">
  						HTML <span class="badge bg-info">new</span>
  					</li>
  					<li class="list-group-item">
  						CSS <span class="badge bg-warning">latest</span>
  					</li>
  					<li class="list-group-item">
  						JavaScript <span class="badge bg-success">advanced</span>
  					</li>
  					<li class="list-group-item">
  						Bootstrap <span class="badge bg-primary">intermediate</span>
  					</li>	
  				</ul>
  		</div>
  	
  </body>
</html>

List Groups With hyperlink
------------------------------
To create a list group with hyper links items use <div> instead of 
<ul> and <a> instead of <li>.

ex:3
------
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  	
  </head>
  <body >
  		
  		<div class="container mt-5">
  				<div class="list-group">
  					<a href="" class="list-group-item">HTML</a>
  					<a href="" class="list-group-item">CSS</a>
  					<a href="" class="list-group-item">JavaScript</a>
  					<a href="" class="list-group-item">Bootstrap</a>
  				</div>
  		</div>
  	
  </body>
</html>

we can use " .active" class to highlight the current item.
We can use ".disabled" class to disabled the link.

ex:4
------
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  	
  </head>
  <body >
  		
  		<div class="container mt-5">
  				<div class="list-group">
  					<a href="" class="list-group-item active">HTML</a>
  					<a href="" class="list-group-item disabled">CSS</a>
  					<a href="" class="list-group-item">JavaScript</a>
  					<a href="" class="list-group-item">Bootstrap</a>
  				</div>
  		</div>
  	
  </body>
</html>


we can add colors to the list group by using below classes.
ex:
.list-group-item-success
.list-group-item-info
.list-group-item-warning and 
.list-group-item-danger:

ex:5
-------
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  	
  </head>
  <body >
  		
  		<div class="container mt-5">
  				<div class="list-group">
  					<a href="" class="list-group-item list-group-item-primary ">HTML</a>
  					<a href="" class="list-group-item list-group-item-info">CSS</a>
  					<a href="" class="list-group-item list-group-item-warning ">JavaScript</a>
  					<a href="" class="list-group-item list-group-item-success">Bootstrap</a>
  				</div>
  		</div>
  	
  </body>
</html>

Bootstrap Collapse Plugin
==============================
Collapsibles are useful when you want to hide and show large amount of content:

A data-bs-toggle="collapse" attribute is used to show and hide the content.

A  data-bs-target attribute is used to connect a button with div tag.

A "collapse" class inside <div> is used to hide/collapse the content for first time.

ex:1
-------
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  	
  </head>
  <body >
  		
  		<div class="container mt-5">
  				
  				<button class="btn btn-primary" data-bs-toggle="collapse" data-bs-target="#myId">
  					Toggle
  				</button>
  				<br><br>
  				<h1 class="collapse" id="myId">
  					This is Bootstrap class for collapse plugin demostration
  				</h1>
  		</div>
  	
  </body>
</html>

ex:2
----
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  	
  </head>
  <body >
  		
  		<div class="container mt-5">
  				
  				<a href="#myId" class="btn btn-primary" data-bs-toggle="collapse" >
  					Toggle
  				</a>
  				<br><br>
  				<h1 class="collapse" id="myId">
  					This is Bootstrap class for collapse plugin demostration
  				</h1>
  		</div>
  	
  </body>
</html>


Bootstrap collapse plugin with Bootstrap cards
===================================================
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  	
  </head>
  <body >
  		
  		<div class="container mt-5">
  				
  				<div class="card">
  					<div class="card-header">
  						<button class="btn btn-outline-primary" data-bs-target="#myId"
  						 data-bs-toggle="collapse">
  							Toggle Button
  						</button>
  					</div>
  					<div class="card-body collapse" id="myId">
  						<div class="row">
  							<div class="col-4">
  								<img src="images/rock.png" class="w-100 h-100 rounded-circle"/>
  							</div>
  							<div class="col-4">
  								<img src="images/rock.png" class="w-100 h-100 rounded-circle"/>
  							</div>
  							<div class="col-4">
  								<img src="images/rock.png" class="w-100 h-100 rounded-circle"/>
  							</div>
  						</div>
  					</div>	
  				</div>
  		</div>
  	
  </body>
</html>












Bootstrap responsive navbar
================================
Navbars require a wrapping .navbar with .navbar-expand{-sm|-md|-lg|-xl|-xxl} for responsive collapsing and color scheme classes.

Navbars and their contents are fluid by default.
Change the container to limit their horizontal width in different ways.

Use our spacing and flex utility classes for controlling spacing and alignment within navbars.

Navbars are responsive by default, but you can easily modify them to change that. 

Responsive behavior depends on our Collapse JavaScript plugin.

.navbar-brand for your company, product, or project name.

.navbar-nav for a full-height and lightweight navigation (including support for dropdowns).

.navbar-toggler for use with our collapse plugin and other navigation toggling behaviors.

Flex and spacing utilities for any form controls and actions.

.navbar-text for adding vertically centered strings of text.

.collapse.navbar-collapse for grouping and hiding navbar contents by a parent breakpoint.

Add an optional .navbar-scroll to set a max-height and scroll expanded navbar content.


ex:1
------
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  	
  </head>
  <body >
  		
  			<nav  class="navbar navbar-expand-lg narbar-light bg-light">
  				<div class="container-fluid">
  					<a href="" class="navbar-brand">IHUB TALENT</a>
  				<button class="navbar-toggler"  data-bs-toggle="collapse" data-bs-target="#myId">
  						<span class="navbar-toggler-icon"></span>
  					</button>

  					<div class="collapse navbar-collapse " id="myId">
  					<ul class="navbar-nav mx-auto">
  						<li class="nav-item ">
  							<a href="" class="nav-link">HOME</a>
  						</li>
  						<li class="nav-item ">
  							<a href="" class="nav-link">ABOUT</a>
  						</li>
  						<li class="nav-item ">
  							<a href="" class="nav-link">SERVICE</a>
  						</li>
  						<li class="nav-item">
  							<a href="" class="nav-link">GALLERY</a>
  						</li>
  						<li class="nav-item">
  							<a href="" class="nav-link">PORTFOLIO</a>
  						</li>
  						<li class="nav-item">
  							<a href="" class="nav-link">CONTACT</a>
  						</li>
  					</ul>	
  					</div>		
  				</div>	
  			</nav>
  			
  	
  </body>
</html>

ex:2
--------
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  	
  </head>
  <body >
  		
  			<nav  class="navbar navbar-expand-lg navbar-dark bg-dark ">
  				<div class="container-fluid">
  					<a href="" class="navbar-brand">IHUB TALENT</a>
  				<button class="navbar-toggler"  data-bs-toggle="collapse" data-bs-target="#myId">
  						<span class="navbar-toggler-icon"></span>
  					</button>

  					<div class="collapse navbar-collapse " id="myId">
  					<ul class="navbar-nav mx-auto">
  						<li class="nav-item ">
  							<a href="" class="nav-link">HOME</a>
  						</li>
  						<li class="nav-item ">
  							<a href="" class="nav-link">ABOUT</a>
  						</li>
  						<li class="nav-item ">
  							<a href="" class="nav-link">SERVICE</a>
  						</li>
  						<li class="nav-item">
  							<a href="" class="nav-link">GALLERY</a>
  						</li>
  						<li class="nav-item">
  							<a href="" class="nav-link">PORTFOLIO</a>
  						</li>
  						<li class="nav-item">
  							<a href="" class="nav-link">CONTACT</a>
  						</li>
  					</ul>	
  					</div>		
  				</div>	
  			</nav>
  			
  	
  </body>
</html>

ex:3
-------
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  	
  </head>
  <body >
  		
  			<nav  class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  				<div class="container">
  					<a href="" class="navbar-brand">IHUB TALENT</a>
  				<button class="navbar-toggler"  data-bs-toggle="collapse" data-bs-target="#myId">
  						<span class="navbar-toggler-icon"></span>
  					</button>

  					<div class="collapse navbar-collapse " id="myId">
  					<ul class="navbar-nav mx-auto">
  						<li class="nav-item ">
  							<a href="" class="nav-link text-center">HOME</a>
  						</li>
  						<li class="nav-item ">
  							<a href="" class="nav-link text-center">ABOUT</a>
  						</li>
  						<li class="nav-item ">
  							<a href="" class="nav-link text-center">SERVICE</a>
  						</li>
  						<li class="nav-item">
  							<a href="" class="nav-link text-center">GALLERY</a>
  						</li>
  						<li class="nav-item">
  							<a href="" class="nav-link text-center">PORTFOLIO</a>
  						</li>
  						<li class="nav-item">
  							<a href="" class="nav-link text-center">CONTACT</a>
  						</li>
  					</ul>	
  					</div>		
  				</div>	
  			</nav>

  			<h1>
  				This is heading tag
  			</h1>
  			<h1>
  				This is heading tag
  			</h1>
  			<h1>
  				This is heading tag
  			</h1>
  			<h1>
  				This is heading tag
  			</h1>
  			<h1>
  				This is heading tag
  			</h1>
  			<h1>
  				This is heading tag
  			</h1>
  			<h1>
  				This is heading tag
  			</h1>
  			<h1>
  				This is heading tag
  			</h1>
  			<h1>
  				This is heading tag
  			</h1>
  			<h1>
  				This is heading tag
  			</h1>
  			<h1>
  				This is heading tag
  			</h1>
  			<h1>
  				This is heading tag
  			</h1>
  			
  	
  </body>
</html>

Bootstrap Scrollspy
====================
The scrollspy plugin automatically highlights the navigation links based on 
the scroll position to indicate where the user is currently on the page.



ex:1
-----
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

    <!-- add external css -->
    <link rel="stylesheet" type="text/css" href="css/mystyles.css">
  	
  </head>
  <body >
  		
  			<nav  class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  				<div class="container">
  					<a href="" class="navbar-brand">IHUB TALENT</a>
  				<button class="navbar-toggler"  data-bs-toggle="collapse" data-bs-target="#myId">
  						<span class="navbar-toggler-icon"></span>
  					</button>

  					<div class="collapse navbar-collapse " id="myId">
  					<ul class="navbar-nav mx-auto">
  						<li class="nav-item ">
  							<a href="#homeId" class="nav-link text-center">HOME</a>
  						</li>
  						<li class="nav-item ">
  							<a href="#aboutId" class="nav-link text-center">ABOUT</a>
  						</li>
  						<li class="nav-item ">
  							<a href="#serviceId" class="nav-link text-center">SERVICE</a>
  						</li>
  						<li class="nav-item">
  							<a href="#galleryId" class="nav-link text-center">GALLERY</a>
  						</li>
  						<li class="nav-item">
  							<a href="#portfolioId" class="nav-link text-center">PORTFOLIO</a>
  						</li>
  						<li class="nav-item">
  							<a href="#contactId" class="nav-link text-center">CONTACT</a>
  						</li>
  					</ul>	
  					</div>		
  				</div>
  			</nav>
  			
  			

  			<!-- section 1-->
  			<section class="home-section" id="homeId">
  				<h1 class="text-center" style="padding-top:200px;">Home Section</h1>
  			</section>

  			<!-- section 2-->
  			<section class="about-section" id="aboutId">
  				<h1 class="text-center" style="padding-top:200px;">ABOUT Section</h1>
  			</section>

  			<!-- section 3-->
  			<section class="service-section" id="serviceId">
  				<h1 class="text-center" style="padding-top:200px;">SERVICE Section</h1>
  			</section>

  			<!-- section 4-->
  			<section class="gallery-section" id="galleryId">
  				<h1 class="text-center" style="padding-top:200px;">GALLERY Section</h1>
  			</section>

  			<!-- section 5-->
  			<section class="portfolio-section" id="portfolioId">
				<h1 class="text-center" style="padding-top:200px;">PORTFOLIO Section</h1>  				
  			</section>

  			<!-- section 1-->
  			<section class="contact-section" id="contactId">
  				<h1 class="text-center" style="padding-top:200px;">CONTACT Section</h1>
  			</section>
  	
  </body>
</html>

mystyles.css
-------------
*
{
	margin: 0;
	padding: 0;
}
.home-section
{
	width: 100%;
	height: 580px;
	background-color: #A3CB38;
}
.about-section
{
	width: 100%;
	height: 590px;
	background-color: #ED4C67;	
}
.gallery-section
{
	width: 100%;
	height: 590px;
	background-color: #EE5A24;
}
.service-section
{
	width: 100%;
	height: 590px;
	background-color: #12CBC4;
}
.portfolio-section
{
	width: 100%;
	height: 590px;
	background-color: #C4E538;
}
.contact-section
{
	width: 100%;
	height: 590px;
	background-color: #B53471;
}



ex:2
------
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

    <!-- add external css -->
    <link rel="stylesheet" type="text/css" href="css/mystyles.css">
  	
  </head>
  <body >
  		
  			<div class="bgimg">
  			<nav  class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  				<div class="container">
  					<a href="" class="navbar-brand">IHUB TALENT</a>
  				<button class="navbar-toggler"  data-bs-toggle="collapse" data-bs-target="#myId">
  						<span class="navbar-toggler-icon"></span>
  					</button>

  					<div class="collapse navbar-collapse " id="myId">
  					<ul class="navbar-nav mx-auto">
  						<li class="nav-item ">
  							<a href="" class="nav-link text-center">HOME</a>
  						</li>
  						<li class="nav-item ">
  							<a href="#aboutId" class="nav-link text-center">ABOUT</a>
  						</li>
  						<li class="nav-item ">
  							<a href="#serviceId" class="nav-link text-center">SERVICE</a>
  						</li>
  						<li class="nav-item">
  							<a href="#galleryId" class="nav-link text-center">GALLERY</a>
  						</li>
  						<li class="nav-item">
  							<a href="#portfolioId" class="nav-link text-center">PORTFOLIO</a>
  						</li>
  						<li class="nav-item">
  							<a href="#contactId" class="nav-link text-center">CONTACT</a>
  						</li>
  					</ul>	
  					</div>		
  				</div>
  			</nav>
  		
  			</div>

  			

  			<!-- section 2-->
  			<section class="about-section" id="aboutId">
  				<h1 class="text-center" style="padding-top:200px;">ABOUT Section</h1>
  			</section>

  			<!-- section 3-->
  			<section class="service-section" id="serviceId">
  				<h1 class="text-center" style="padding-top:200px;">SERVICE Section</h1>
  			</section>

  			<!-- section 4-->
  			<section class="gallery-section" id="galleryId">
  				<h1 class="text-center" style="padding-top:200px;">GALLERY Section</h1>
  			</section>

  			<!-- section 5-->
  			<section class="portfolio-section" id="portfolioId">
				<h1 class="text-center" style="padding-top:200px;">PORTFOLIO Section</h1>  				
  			</section>

  			<!-- section 1-->
  			<section class="contact-section" id="contactId">
  				<h1 class="text-center" style="padding-top:200px;">CONTACT Section</h1>
  			</section>
  	
  </body>
</html>






Bootstrap carousel
====================
A .carousel class creates a carousel

A .slide class adds a CSS transition and animation effect when sliding from one item to the next. Remove this class if you do not want this effect.

A .carousel-inner class adds slides to the carousel.

A .carousel-item class specifies the content of each slide.

Add elements inside <div class="carousel-caption"> within each <div class="carousel-item"> to create a caption for each slide.

A .carousel-indicators class adds indicators for the carousel. 
These are the little dots at the bottom of each slide (which indicates how many slides there are in the carousel, and which slide the user are currently viewing).


A .carousel-control-prev class adds a left (previous) button to the carousel, which allows the user to go back between the slides.

A .carousel-control-next class adds a right (next) button to the carousel, which allows the user to go forward between the slides.

A .carousel-control-prev-icon class used together with .carousel-control-prev to create a "previous" button.

A .carousel-control-next-icon class used together with .carousel-control-next to create a "next" button.


ex:
------
<!doctype html>
<html lang="en">
  <head>
  	<title>IHUB TALENT</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  	
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

 	<!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

  </head>
  <body >
  		
  		<div class="container mt-3">
  			
  	<div id="demo" class="carousel slide" data-bs-ride="carousel">


          <!-- Indicators/dots -->
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#demo" data-bs-slide-to="0" class="active"></button>
    <button type="button" data-bs-target="#demo" data-bs-slide-to="1"></button>
    <button type="button" data-bs-target="#demo" data-bs-slide-to="2"></button>
  </div>
  		


  				<div class="carousel-inner">
  					<div class="carousel-item active">
  						<img src="images/image1.jpg" class="w-100 mx-auto d-block"/>
  						<div class="carousel-caption">
          					<h3>Los Angeles</h3>
          					<p>We had such a great time in LA!</p>
        				</div>  
  					</div>
  					<div class="carousel-item">
  						<img src="images/image2.jpg" class="w-100 mx-auto d-block"/>
  						 <div class="carousel-caption">
          					<h3>Chicago</h3>
          					<p>We had a pop up party!</p>
        				</div> 
  					</div>
  					<div class="carousel-item">
  						<img src="images/image3.jpg" class="w-100 mx-auto d-block"/>
  						<div class="carousel-caption">
          					<h3>Washington DC</h3>
          					<p>Lets start a new celebration!</p>
        				</div> 
  					</div>
  				</div>	

  				<!-- Left and right controls/icons -->
  				<button class="carousel-control-prev" 
  						type="button" data-bs-target="#demo" data-bs-slide="prev">
    						<span class="carousel-control-prev-icon"></span>
 				 </button>
  				<button class="carousel-control-next"
  				 		type="button" data-bs-target="#demo" data-bs-slide="next">
    						<span class="carousel-control-next-icon"></span>
 				 </button>

  			</div>

  			


  		</div>
  			
  </body>
</html>

































































