CSS3
======
CSS stands for Cascading Styles Sheet.

It is widely used language on web like HTML.

To learn CSS we must have fundamental knowledge on HTML.

The latest version CSS3 was introduced in 2001.

All CSS document we must save with ".css" extension.

CSS is used to apply the styles on HTML elements/tags.

Using CSS we can do following operations.

1) Positioning of an element.

2) It applies the styles do describe how an element should look like.

3) It performs some sort of animation.

Advantages
=============
1)It is easy to learn and easy to use.

2)Flexibility 

3)It saves lot of development time

4)It is compatible with multiple browsers

5)It supports Global change

6)We can maintain HTML code and CSS code seperately.

Disadvantages
==============
1)Fragmentation 

2)Need to update all the versions of CSS.


CSS sample example
==================
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
		<style  type="text/css">
			-
			-  // set of properties 
			-
		</style>
	</head>
	<body>
		<h1>This is Heading Tag</h1>	
	</body>
</html>


ex:1
------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
		<style type="text/css">
			h1
			{
				color:red;
			}
		</style>
	</head>
	<body>
		<h1>This is Heading Tag</h1>	
	</body>
</html>

Note:
----
	Here styles are cascade from head to body.
	

ex:2
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
		<style>
			h1
			{
				color:#FF0000;
			}
		</style>
	</head>
	<body>
		<h1>This is Heading Tag</h1>	
	</body>
</html>

We can declare multiple properties for a single element.

ex:3
------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
		<style>
			h1
			{
				color:#FF0000;
				background-color:yellow;
				text-align:center;
				font-size:90px;
			}
		</style>
	</head>
	<body>
		<h1>This is Heading Tag</h1>	
	</body>
</html>


CSS syntax
============
CSS rules set contains selector and declaration block.

ex:
	selector 
	|
	h1 { color:red;background-color:yellow; }
	   |____________________________________|
			    |
		Declaration Block 

In CSS , declaration block contains property name and property value seperated with colon(:) and Each property must be seperated with semicolon (;).

Selector indicates which element we need to apply the styles.

Types of CSS 
=================
We have three types of styles in CSS.

1)Inline CSS

2)Internal CSS / Embeded  CSS 

3)External CSS / Seperate CSS 


1)Inline CSS
--------------
If we want to apply unique styles on single element then we need to use inline CSS.

Using "style" attribute we can achive inline CSS.

ex:1
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>

	</head>
	<body>
		<h1  style="color:blue;background-color:yellow">This is Heading Tag</h1>	
	</body>
</html>

ex:2
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>

	</head>
	<body>
		<h1  style="color:blue;background-color:yellow">
			This is Heading Tag
		</h1>
		<h1  style="text-align:center;color:red;">
			This is Heading Tag
		</h1>
		<h1  style="font-size:70px;">
			This is Heading Tag
		</h1>		
	</body>
</html>


2)Internal CSS / Embeded  CSS 
----------------------------
If we want to apply the unique styles on a single web page then we need to use 
internal CSS.

Using <style> attribute we can achieve internal CSS.

ex:1
----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
		<style  type="text/css">
			h1
			{
				color: rgb(0,0,255);
				background-color:#C3C3C3;
			}
		</style>
	</head>
	<body>
		<h1>
			This is Heading Tag
		</h1>
			
	</body>
</html>


ex:2
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
		<style  type="text/css">
			h1
			{
				color: rgb(0,0,255);
				background-color:#C3C3C3;
				text-align:center;
			}
		</style>
	</head>
	<body>
		<h1>
			This is Heading Tag
		</h1>
		<h1>
			This is Heading Tag
		</h1>
		<h1>
			This is Heading Tag
		</h1>
			
	</body>
</html>

External CSS
==============
External CSS allows to make a global change which reflects to entire website(collection of web pages).

In external css we need to create two files i.e .html file and .css file.

HTML code we need to declare in .html file and CSS code we need to declare in
.css file.

But We need load .CSS file in .html file by using <link> tag inside <head> tag.

Diagram: class10.1

ex:1
------

index.html
------------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
		<!-- adding external css -->
		<link rel="stylesheet" type="text/css" href="mystyles.css">
	</head>
	<body>
		<h1>This is CSS class</h1>
	</body>
</html>

mystyles.css
-----------
h1
{
	color:blue;
	font-size:70px;
	text-align:center;
}

ex:2
-----

index.html
------------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB TALENT</title>
		<!-- adding external css -->
		<link rel="stylesheet" type="text/css" href="mystyles.css">
	</head>
	<body>
		<h1>This is CSS class</h1>
	</body>
</html>

mystyles.css
------------
body
{
	background-color:lightgray;
}
h1
{
	color:blue;
	font-size:70px;
	text-align:center;
}








































































































