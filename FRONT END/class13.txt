Q) Types of selectors in CSS ?

We have five selectors in CSS.

1)element selector

2)Id selector

3)class selector 

4)group selector

5)universal selector 


1)element selector
------------------
The element selector selects HTML elements based on element name

ex:1
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			
			/* element selector */	
			h1
			{
				color: #FF0000;	
				text-align: center;
			}
		</style>
	</head>
	<body>
		<h1>Heading Tag</h1>
	</body>
</html>

ex:2
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			
			/* element selector */	
			h1
			{
				color: #FF0000;	
				text-align: center;
			}
	
		</style>
	</head>
	<body>
		<h1>Heading Tag</h1>
		<h1>Heading Tag</h1>
	</body>
</html>



2)id selector
--------------
The id selector uses the id attribute of an HTML element to select a
specific element.

The id of an element is unique within a page, so the id selector is used to
select one unique element.

To select an element with a specific id, write a hash (#) character,
followed by the id of the element.

ex:1
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			
			/* id selector */	
			#myId
			{
				color: #FF0000;	
				text-align: center;
			}

		</style>
	</head>
	<body>
		<h1  id="myId">Heading Tag</h1>
	</body>
</html>


ex:2
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			
			/* id selector */	
			#myId
			{
				color: #FF0000;	
				text-align: center;
			}

		</style>
	</head>
	<body>
		<h1  id="myId">Heading Tag</h1>
		<h1>Heading Tag</h1>
	</body>
</html>

If we want to apply unique style only on single element then 
we need to use id selector.

<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			
			/* id selector */	
			#myId1
			{
				color: #FF0000;	
				text-align: center;
			}
			#myId2
			{
				font-size: 40px;
				font-weight: bold;
				background-color: yellow;
			}

		</style>
	</head>
	<body>
		<h1  id="myId1">Heading Tag</h1>
		<p id="myId2">Heading Tag</p>
	</body>
</html>


We can't use multiple id's in a single element.

ex:

<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			
			/* id selector */	
			#myId1
			{
				color: #FF0000;	
				text-align: center;
			}
			#myId2
			{
				font-size: 40px;
				font-weight: bold;
				background-color: yellow;
			}

		</style>
	</head>
	<body>
		<h1  id="myId1">Heading Tag</h1>
		<h1  id="myId2">Heading Tag</h1>
		<h1  id="myId1  myId2">Heading Tag</h1>
	</body>
</html>

3)class selector
--------------------
The class selector selects HTML elements with a specific class attribute.

To select elements with a specific class, write a period (.) character,
followed by the class name.

ex:1
----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			
			/* class selector */	
			.myClass
			{
				color: #FF0000;	
				text-align: center;
			}
			
		</style>
	</head>
	<body>
		<h1 class="myClass">Heading Tag</h1>
		<h1>Heading Tag</h1>
		<h1>Heading Tag</h1>
	</body>
</html>

If we want to apply the unique style on multiple elements then we need to use class selector.

ex:
----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			
			/* class selector */	
			.myClass
			{
				color: #FF0000;	
				text-align: center;
			}
			
		</style>
	</head>
	<body>
		<h1 class="myClass">Heading Tag</h1>
		<h1 class="myClass">Heading Tag</h1>
		<h1 class="myClass">Heading Tag</h1>
	</body>
</html>

We can use multiple classes on single element.

ex:
----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			
			/* class selector */	
			.myClass1
			{
				color: #FF0000;	
				text-align: center;
			}
			.myClass2
			{
				background-color: yellow;
			}
			
		</style>
	</head>
	<body>
		<h1 class="myClass1">Heading Tag</h1>
		<h1 class="myClass2">Heading Tag</h1>
		<h1 class="myClass1  myClass2">Heading Tag</h1>
	</body>
</html>

4)group selector
-----------------
The grouping selector selects all the HTML elements with the same style
definitions.

ex:
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			
			/* group selector */	
			p,div,h1
			{
				color: blue;
				background-color: yellow;
				text-align: center;
			}
			
		</style>
	</head>
	<body>
		<p>Paragraph tag</p>
		<div>Division tag</div>
		<h1>Heading tag</h1>
	</body>
</html>


5)Universal selector
---------------------
The universal selector (*) selects all HTML elements on the page

ex:
----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			
			/* universal selector */	
			*
			{
				color: blue;
				background-color: yellow;
				text-align: center;
			}
			
		</style>
	</head>
	<body>
		<p>Paragraph tag</p>
		<div>Division tag</div>
		<h1>Heading tag</h1>
	</body>
</html>


CSS overflow property
=====================
The overflow property specifies what should happen if content overflow.

This property spcifies weither to clip content or to add scrollbars  when an element content is too big to fit in a specified area.

Note:
---------
The overflow property works for block elements with  a specified height.

value		Description
---------	-----------------
visible		The overflow is not clipped.It rendered outside the element 's box and 
		it is default value.

hidden		the overflow is clipped  and rest of the content will be invisible.

scroll		The overflow is clipped,but a scroll-bar is added to see the rest of the 
		content .

auto		The overflow is clipped,a scroll-bar should be added to the rest of the 
		content.


overflow: visible
-----------------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#myId
			{
				width: 200px;
				height: 200px;	
				border: 2px solid black;
				overflow: visible;
			}
			
		</style>
	</head>
	<body>
		<div id="myId">
			Web Technology refers to the various tools and techniques that are utilized in the process of communication between different types of devices over the internet. A web browser is used to access web pages. Web browsers can be defined as programs that display text, data, pictures, animation, and video on the Internet.
			Web Technology refers to the various tools and techniques that are utilized in the process of communication between different types of devices over the internet. 
		</div>
	</body>
</html>


overflow:hidden
---------------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#myId
			{
				width: 200px;
				height: 200px;	
				border: 2px solid black;
				overflow: hidden;
			}
			
		</style>
	</head>
	<body>
		<div id="myId">
			Web Technology refers to the various tools and techniques that are utilized in the process of communication between different types of devices over the internet. A web browser is used to access web pages. Web browsers can be defined as programs that display text, data, pictures, animation, and video on the Internet.
			Web Technology refers to the various tools and techniques that are utilized in the process of communication between different types of devices over the internet. 
		</div>
	</body>
</html>

overflow:scroll
---------------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#myId
			{
				width: 200px;
				height: 200px;	
				border: 2px solid black;
				overflow: scroll;
			}
			
		</style>
	</head>
	<body>
		<div id="myId">
			Web Technology refers to the various tools and techniques that are utilized in the process of communication between different types of devices over the internet. A web browser is used to access web pages. Web browsers can be defined as programs that display text, data, pictures, animation, and video on the Internet.
			Web Technology refers to the various tools and techniques that are utilized in the process of communication between different types of devices over the internet. 
		</div>
	</body>
</html>


overflow:auto
-------------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#myId
			{
				width: 200px;
				height: 200px;	
				border: 2px solid black;
				overflow: auto;
			}
			
		</style>
	</head>
	<body>
		<div id="myId">
			Web Technology refers to the various tools and techniques that are utilized in the process of communication between different types of devices over the internet. A web browser is used to access web pages.
		</div>
	</body>
</html>

overflow-x:auto
------------------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#myId
			{
				width: 200px;
				height: 200px;	
				border: 2px solid black;
				overflow-x: auto;
			}
			
		</style>
	</head>
	<body>
		<div id="myId">
			<img src="images/rock.png" width="900px" height="150px"/>
		</div>
	</body>
</html>

overflow-y:auto
------------------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#myId
			{
				width: 200px;
				height: 200px;	
				border: 2px solid black;
				overflow-y: auto;
			}
			
		</style>
	</head>
	<body>
		<div id="myId">
			<img src="images/rock.png" width="150px" height="600px"/>
		</div>
	</body>
</html>


CSS border-radius property
=======================
The border-radius property defines  the radius of the element's corners.

This property allows us to add rounded borders to elements.

This property can have from one to four values.
ex:
	border-top-left-radius
	border-top-right-radius
	border-bottom-right-radius
	border-bottom-left-radius
	or 
	border-radius 

ex:1
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#myId
			{
				width: 200px;
				height: 200px;	
				border: 4px solid black;
				overflow: auto;
				margin: 80px auto;
				border-top-left-radius: 5px;
				border-top-right-radius: 10px;
				border-bottom-right-radius:15px;
				border-bottom-left-radius: 20px;
			}
			
		</style>
	</head>
	<body>
		<div id="myId">
			
		</div>
	</body>
</html>


ex:2
----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#myId
			{
				width: 200px;
				height: 200px;	
				border: 4px solid black;
				overflow: auto;
				margin: 80px auto;
				border-radius: 5px 10px 15px 20px;
			}
			
		</style>
	</head>
	<body>
		<div id="myId">
			
		</div>
	</body>
</html>


ex:3
------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#myId
			{
				width: 200px;
				height: 200px;	
				border: 4px solid black;
				overflow: auto;
				margin: 80px auto;
				/*topright-bottom-left & topleft-bottomright*/;
				border-radius: 10px 30px;
			}
			
		</style>
	</head>
	<body>
		<div id="myId">
			
		</div>
	</body>
</html>


ex:4
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#myId
			{
				width: 200px;
				height: 200px;	
				border: 4px solid black;
				overflow: auto;
				margin: 80px auto;
				border-radius: 10px;
			}
			
		</style>
	</head>
	<body>
		<div id="myId">
			
		</div>
	</body>
</html>


CSS box-shadow property
========================
The box-shadow property attaches one or more shadows to an element.

syntax
--------
	box-shadow: none |h-offset  v-offset  blur spread  color
ex:
	box-shadow: 2px 2px 3px 10px blue; 


ex:1
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#myId
			{
				width: 200px;
				height: 200px;	
				overflow: auto;
				margin: 80px auto;
				border-radius: 10px;
				box-shadow: 2px 2px 18px 6px  #FF0000;
			}
			
		</style>
	</head>
	<body>
		<div id="myId">
			
		</div>
	</body>
</html>

ex:2
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#myId
			{
				width: 200px;
				height: 200px;	
				overflow: auto;
				margin: 80px auto;
				border-radius: 10px;
				box-shadow: 2px 2px 18px 6px  #FF0000 inset;
			}
			
		</style>
	</head>
	<body>
		<div id="myId">
			
		</div>
	</body>
</html>


ex:3
-------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#myId
			{
				width: 200px;
				height: 200px;	
				overflow: auto;
				margin: 80px auto;
				border-radius: 10px;
				box-shadow: 2px 2px 18px 6px  #FF0000 , 4px 4px 22px 6px #FFFF00;
			}
			
		</style>
	</head>
	<body>
		<div id="myId">
			
		</div>
	</body>
</html>


CSS float property
==================
It is widely used property on a web page.

The float property specifies how an element should float.

value		Description
-----------		-----------------
none		The element does not float.

left		the element floats to the left of its container.

right		The element floats to the right of its container.






float:none
-----------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#box1
			{
				width: 200px;
				height: 200px;
				background-color: red;
				float: none;
			}
			#box2
			{
				width: 200px;
				height: 200px;
				background-color: blue;	
				float: none;
			}
			
		</style>
	</head>
	<body>
		<div id="box1"></div>
		<div id="box2"></div>
	</body>
</html>


float:left
----------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#box1
			{
				width: 200px;
				height: 200px;
				background-color: red;
				float: left;
			}
			#box2
			{
				width: 200px;
				height: 200px;
				background-color: blue;	
				float: left;
			}
			
		</style>
	</head>
	<body>
		<div id="box1"></div>
		<div id="box2"></div>
	</body>
</html>


float:right
----------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#box1
			{
				width: 200px;
				height: 200px;
				background-color: red;
				float: left;
			}
			#box2
			{
				width: 200px;
				height: 200px;
				background-color: blue;	
				float: right;
			}
			
		</style>
	</head>
	<body>
		<div id="box1"></div>
		<div id="box2"></div>
	</body>
</html>

ex:
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
		<style type="text/css">
			#box1
			{
				width: 200px;
				height: 200px;
				background-color: red;
				float: right;
			}
			#box2
			{
				width: 200px;
				height: 200px;
				background-color: blue;	
				float: right;
			}
			
		</style>
	</head>
	<body>
		<div id="box1"></div>
		<div id="box2"></div>
	</body>
</html>


Sample CSS design
==================

Design1 (folder)
|
|------css (folder)
|	|
	|-----mystyles.css
|
|------images
	|
	|-----rock.png
|
|------index.html
|


Diagram: class13.1

































































































































































































































































































































































































 







































































