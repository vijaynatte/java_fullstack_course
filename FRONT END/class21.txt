Javascript switch case
========================
It is used to evaluate the code based on multiple conditions.

It is similar to if else if statement.

It is more convenient when compare to if else if because we can declare numbers,Strings and float.

In switch case break statement is optional.If we won't use break statement then from where our condition is satisfied from there all cases will be executed.

syntax:
	switch(condition)
	{
		case  val1:  
				//code to be execute
				break stmt;
		case val2:
				//code to be execute
				break stmt;

		default:
				//code to be execute
	}

ex:1
------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">
				var val=prompt("Enter the option :");
				var option=parseInt(val);
				switch(option)
				{
					case 100: document.writeln("It is police number");
							  break;

					case 103: document.writeln("It is enquiry number");
							  break;

					case 108: document.writeln("It is Emergency number");
							  break;

					default:  document.writeln("Invalid option");
				}		
		</script>
	
	</body>
	</body>
</html>

ex:2
----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">
				var val=prompt("Enter the option :");
				var option=parseInt(val);
				switch(option)
				{
					case 100: document.writeln("It is police number");
							  //break;

					case 103: document.writeln("It is enquiry number");
							  //break;

					case 108: document.writeln("It is Emergency number");
							  //break;

					default:  document.writeln("Invalid option");
				}		
		</script>
	
	</body>
	</body>
</html>

ex:3
------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">
				var ch=prompt("Enter the Alphabet:");
				
				switch(ch)
				{
					case 'a': document.writeln("It is a vowel");break;

					case 'e': document.writeln("It is a vowel");break;
					
					case 'i': document.writeln("It is a vowel");break;

					case 'o': document.writeln("It is a vowel");break;

					case 'u': document.writeln("It is a vowel");break;

					default:  document.writeln("It is not a vowel");
				}		
		</script>
	
	</body>
	</body>
</html>

ex:4
------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">
				var str=prompt("Enter the String:");
				
				switch(str)
				{
					case "one": document.writeln("January");break;

					case "two": document.writeln("February");break;
					
					case "three": document.writeln("March");break;

					case "four": document.writeln("April");break;

					case "five": document.writeln("May");break;

					default:  document.writeln("coming soon");
				}		
		</script>
	
	</body>
	</body>
</html>

ex:5
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">
				var val=prompt("Enter the Number :");
				var n=parseFloat(val);
				switch(n)
				{
					case 10.1: document.writeln("stmt1");break;

					case 10.2: document.writeln("stmt2");break;
					
					case 10.3: document.writeln("stmt3");break;

					case 10.4: document.writeln("stmt4");break;

					case 10.5: document.writeln("stmt5");break;

					default:  document.writeln("coming soon");
				}		
		</script>
	
	</body>
	</body>
</html>

Javascript LOOPS
=================
It is used to iteration the piece of code using do while loop,while loop ,for loop and for in loop.

In javascript, we have four types of loops.

1) do while loop

2) while loop

3) for loop

4) for in loop

1) do while loop
-------------------
It will evaluate the code untill our condition is true.

syntax:

	do
	{
		-
		- // stmts to execute
		-
	}while(condition);

In do while loop, our code will execute atleast for one time either our condition is true or false.

ex:1
------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">
			var i=1;
			do
			{
				document.writeln(i+" ");//1
				i++;
			}while(i<=10);
				
		</script>
	
	</body>
	</body>
</html>

ex:2
------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">
			var i=10;
			do
			{
				document.writeln(i+" ");//
				i--;
			}while(i>=1);

		</script>
	
	</body>
	</body>
</html>


Q)Write a javascript program to perform sum of 10 natural numbers?

1+2+3+4+5+6+7+8+9+10= 55

ex:

<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">
			var i=1,sum=0;
			do
			{
				sum+=i;  // sum = sum + i ;
				i++;
			}while(i<=10);

			document.writeln("sum of 10 natural numbers is ="+sum);

		</script>
	
	</body>
	</body>
</html>

Q)Write a javascript program to find out factorial of a given number?

Input:
	n = 5;

Output:
	5*4*3*2*1 = 120

ex:
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">
			var val=prompt("Enter the Number :");
			var n=parseInt(val);

			var i=n,fact=1;
			do
			{
				fact*=i;
				i--;
			}while(i>=1);

			document.writeln("Factorial of a given number is ="+fact);

		</script>
	
	</body>
	</body>
</html>

Q)Write a javascript program to display multiplication table of a given number?

ex:
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">
			var val=prompt("Enter the Number :");
			var n=parseInt(val);

			var i=1;
			do
			{
				document.writeln(n+" * "+i+" = "+n*i+"<br>");	
				i++;
			}while(i<=10);

		</script>
	
	</body>
	</body>
</html>


2)while loop
----------------
It will evaluate the code untill our condition is true.

syntax:
	while(condition)
	{
		-
		- // code to evaluate
		-
	}

ex:1
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">
			var i=1;
			while(i<=10)
			{
				document.writeln(i+" ");
				i++;	
			}
		</script>
	
	</body>
	</body>
</html>

ex:2
-------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">
			var i=1,sum=0;
			while(i<=10)
			{
				sum=sum+i;
				i++;	
			}
			document.writeln("sum of 10 natural numbers is ="+sum);
		</script>
	
	</body>
	</body>
</html>

ex:3
----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">

			var val=prompt("Enter the Number :");
			var n=parseInt(val);

			var i=n,fact=1;
			while(i>=1)
			{
				fact=fact*i;
				i--;	
			}
			document.writeln("Factorial of a given number is ="+fact);
		</script>
	
	</body>
	</body>
</html>


ex:4
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">

			var val=prompt("Enter the Number :");
			var n=parseInt(val);

			var i=1;
			while(i<=10)
			{
				document.writeln(n+" * "+i+" = "+n*i+"<br>");	
				i++;
			}
		
		</script>
	
	</body>
	</body>
</html>

3)for loop
============
It will evaluate the code untill our condition is true.

syntax:
	for(initialization;condition;incrementation/decrementation)
	{
		-
		- // code to be evaluate
		-
	}

ex:
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">

			for(var i=1;i<=10;i++)
			{
				document.writeln(i+" ");
			}
		
		</script>
	
	</body>
	</body>
</html>

ex:
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">

			var sum=0;
			for(var i=1;i<=10;i++)
			{
				sum+=i;
			}
			document.writeln("sum of 10 natural numbers is ="+sum);
		</script>
	
	</body>
	</body>
</html>


ex:
----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">

			var n=parseInt(prompt("Enter the Number :"));

			var fact=1;
			for(var i=n;i>=1;i--)
			{
				fact*=i;
			}
			document.writeln("Factorial of a given number is ="+fact);
		</script>
	
	</body>
	</body>
</html>


ex:4
------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">

			var n=parseInt(prompt("Enter the Number :"));

			for(var i=1;i<=10;i++)
			{
				document.writeln(n+" * "+i+" = "+n*i+"<br>");	
			}
			
		</script>
	
	</body>
	</body>
</html>

Note:
-------
If we know number of iterations then we need to use for loop.

If we don't know number of iterations then we need to use while loop.

If we don't know number of iterations but code must be execute atleast for one time 
then we need to use do while loop.

4)for IN loop
=---------------
It is used to iterate the data from array.

ex:1
-----
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">

			var arr=[10,20,30,40];

			for(var i in arr)
			{
				document.writeln(arr[i]+" ");					
			}
			
		</script>
	
	</body>
	</body>
</html>

ex:2
-------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">

			var arr=['a','b','c','d'];

			for(var i in arr)
			{
				document.writeln(arr[i]+" ");					
			}
			
		</script>
	
	</body>
	</body>
</html>


ex:3
------
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">

			var arr=["HTML","CSS","JS","REACT"];

			for(var i in arr)
			{
				document.writeln(arr[i]+" ");					
			}
			
		</script>
	
	</body>
	</body>
</html>



Program
=========
Q)Write a javascript program to find out number of even's in 10 natural numbers?

10 natural numbers : 1 2 3 4 5 6 7 8 9 10

No of even's 	: 5

ex:
<!DOCTYPE html>
<html>
	<head>
		<title>IHUB Talent</title>
	</head>
	<body>
		
		<script type="text/javascript">

			var cnt=0;
			for(var i=1;i<=10;i++)
			{
				if(i%2==0)
				{
					cnt++;
				}
			}
			document.writeln("Number of evens in 10 natural numbers is ="+cnt);
		</script>
	
	</body>
	</body>
</html>

Q)Write a javascript program to accept two numbers ? Then display multiplication tables in between the numbers if first number is less then second number?

Inputs:
	 2     4 
Outputs:
	2*1=2
	--
	3*1=3
	-
	4*1=4
	-
	


















































































































































































































































































































































































































































































































