MIME Types / setContentType()
==================================
MIME stands for Multipurpose Internet Mail Extension.

We can display the output in servlet in following formats.

1)text/html
------------
	It will display the output in html format.

2)text/xml
-----------
	It will display the output in xml format.

3)application/ms-word
----------------------
	It will display the output in word format.

4)application/vnd.ms-excel
-----------------------
	It will display the output in excel format.


Deployment Directory Structure
-----------------------------
MIMEApp
|
|------Java Resources
|		|
		|------src
|			|
			|----com.ihub.www
				|
				|---TestSrv1.java
				|---TestSrv2.java
				|---TestSrv3.java
				|---TestSrv4.java
|
|
|------Web content 
|		|
		|------WEB-INF
			|
			|------web.xml
|

Note:
-----
In above application we need to add "servlet-api.jar" file in project build path.


TestSrv1.java
-------------
package com.ihub.www;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class TestSrv1 extends GenericServlet
{
	public void service(ServletRequest req,ServletResponse res)throws ServletException,IOException
	{
		PrintWriter pw=res.getWriter();
		res.setContentType("text/html");
		
		pw.println("<table border='1'>");
		pw.println("<tr><th>SNO</th><th>SNAME</th><th>SADD</th></tr>");
		pw.println("<tr><td>101</td><td>Alan</td><td>Florida</td></tr>");
		pw.println("<tr><td>102</td><td>Jose</td><td>Texas</td></tr>");
		pw.println("<tr><td>103</td><td>Kelvin</td><td>Vegas</td></tr>");
		pw.println("</table>");
		
		pw.close();
	}
}


TestSrv2.java
--------------
package com.ihub.www;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class TestSrv2 extends GenericServlet
{
	public void service(ServletRequest req,ServletResponse res)throws ServletException,IOException
	{
		PrintWriter pw=res.getWriter();
		res.setContentType("text/xml");
		
		pw.println("<table border='1'>");
		pw.println("<tr><th>SNO</th><th>SNAME</th><th>SADD</th></tr>");
		pw.println("<tr><td>101</td><td>Alan</td><td>Florida</td></tr>");
		pw.println("<tr><td>102</td><td>Jose</td><td>Texas</td></tr>");
		pw.println("<tr><td>103</td><td>Kelvin</td><td>Vegas</td></tr>");
		pw.println("</table>");
		
		pw.close();
	}
}


TestSrv3.java
-------------
package com.ihub.www;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class TestSrv3 extends GenericServlet
{
	public void service(ServletRequest req,ServletResponse res)throws ServletException,IOException
	{
		PrintWriter pw=res.getWriter();
		res.setContentType("application/ms-word");
		
		pw.println("<table border='1'>");
		pw.println("<tr><th>SNO</th><th>SNAME</th><th>SADD</th></tr>");
		pw.println("<tr><td>101</td><td>Alan</td><td>Florida</td></tr>");
		pw.println("<tr><td>102</td><td>Jose</td><td>Texas</td></tr>");
		pw.println("<tr><td>103</td><td>Kelvin</td><td>Vegas</td></tr>");
		pw.println("</table>");
		
		pw.close();
	}
}


TestSrv4.java
--------------
package com.ihub.www;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class TestSrv4 extends GenericServlet
{
	public void service(ServletRequest req,ServletResponse res)throws ServletException,IOException
	{
		PrintWriter pw=res.getWriter();
		res.setContentType("application/vnd.ms-excel");
		
		pw.println("<table border='1'>");
		pw.println("<tr><th>SNO</th><th>SNAME</th><th>SADD</th></tr>");
		pw.println("<tr><td>101</td><td>Alan</td><td>Florida</td></tr>");
		pw.println("<tr><td>102</td><td>Jose</td><td>Texas</td></tr>");
		pw.println("<tr><td>103</td><td>Kelvin</td><td>Vegas</td></tr>");
		pw.println("</table>");
		
		pw.close();
	}
}

Note:
-------
	If a web application contains multiple servlet programs then each 
	servlet program we need to configure in web.xml file using various 
	<servlet> and <servlet-mapping> tag.


web.xml
--------
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee" xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd" id="WebApp_ID" version="3.0">
  
  <servlet>
  	<servlet-name>TestSrv1</servlet-name>
  	<servlet-class>com.ihub.www.TestSrv1</servlet-class>
  </servlet>
  
  <servlet-mapping>
  	<servlet-name>TestSrv1</servlet-name>
  	<url-pattern>/html</url-pattern>
  </servlet-mapping>
  
   <servlet>
  	<servlet-name>TestSrv2</servlet-name>
  	<servlet-class>com.ihub.www.TestSrv2</servlet-class>
  </servlet>
  
  <servlet-mapping>
  	<servlet-name>TestSrv2</servlet-name>
  	<url-pattern>/xml</url-pattern>
  </servlet-mapping>
  
  
   <servlet>
  	<servlet-name>TestSrv3</servlet-name>
  	<servlet-class>com.ihub.www.TestSrv3</servlet-class>
  </servlet>
  
  <servlet-mapping>
  	<servlet-name>TestSrv3</servlet-name>
  	<url-pattern>/word</url-pattern>
  </servlet-mapping>
  
   <servlet>
  	<servlet-name>TestSrv4</servlet-name>
  	<servlet-class>com.ihub.www.TestSrv4</servlet-class>
  </servlet>
  
  <servlet-mapping>
  	<servlet-name>TestSrv4</servlet-name>
  	<url-pattern>/excel</url-pattern>
  </servlet-mapping>
</web-app>

request url
----------
	http://localhost:2525/MIMEApp/html
	http://localhost:2525/MIMEApp/xml
	http://localhost:2525/MIMEApp/word
	http://localhost:2525/MIMEApp/excel



Types of Communication
========================
We can communicate to servlet program in three ways.

1)Browser to servlet communication

2)HTML to servlet communication

3)Servlet to servlet communication 

In browser to servlet communication we need to type our request url in browser address bar.But typing request url in browser address bar is quit complex.

To overcome this limitation we need to use HTML to servlet communication.

In html to servlet communication we can generate the request by using html based 
hyperlinks and form pages.

The request which is generated by using hyperlink does not carry the data.

But the request which is generated by using form page will carry the data.

In html based hyper link to servlet communication we need to type our request url 
as href url.
ex:
	<a href="http://localhost:2525/DateApp/test"> clickMe </a>

In html based form page to servlet communication we need to type our request url
as action url.
ex:
	<form  action="http://localhost:2525/DateApp/test">
		-
		-
		-
	</form>

Example application on HTML based Hyperlink to Servlet Communication
=====================================================================
Diagram: servlet3.1

Deployment Directory Structure
-----------------------------
WishApp
|
|------Java Resources
|		|
		|------src
|			|
			|----com.ihub.www
				|
				|---WishSrv.java
|
|
|------Web content 
		|
		|-----index.html
|		|
		|------WEB-INF
			|
			|------web.xml
|

Note:
-----
In above application we need to add "servlet-api.jar" file in project build path.


It is never recommanded to extends a servlet class with GenericServlet class because it does not give HTTP protocol features.

It is always recommanded to extends our class with HttpServlet class because 
it gives HTTP protocol features.

HttpServlet class present in javax.servlet.http package.

index.html
---------
<center>
	<h1>
		<a href="test">getMsg</a>
	</h1>
</center>


web.xml
---------
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee" xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd" id="WebApp_ID" version="3.0">
  
  <servlet>
  	<servlet-name>WishSrv</servlet-name>
  	<servlet-class>com.ihub.www.WishSrv</servlet-class>
  </servlet>
  
  <servlet-mapping>
  	<servlet-name>WishSrv</servlet-name>
  	<url-pattern>/test</url-pattern>
  </servlet-mapping>
  
  <welcome-file-list>
  	<welcome-file>index.html</welcome-file>
  </welcome-file-list>
  
</web-app>


WishSrv.java
------------
package com.ihub.www;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class WishSrv extends HttpServlet 
{
	public void service(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
	{
		PrintWriter pw=res.getWriter();
		res.setContentType("text/html");
		
		Calendar c=Calendar.getInstance();
		int h=c.get(Calendar.HOUR_OF_DAY);
		if(h<12)
			pw.println("<center><h1>Good Morning</h1></center>");
		else if(h<16)
			pw.println("<center><h1>Good Afternoon</h1></center>");
		else if(h<20)
			pw.println("<center><h1>Good Evening</h1></center>");
		else
			pw.println("<center><h1>Good Night</h1></center>");
		
		pw.close();
	}
}


Request url
--------
	http://localhost:2525/WishApp/

Example application on HTML based Form page to Servlet communication
====================================================================

Diagram: servlet3.2


Deployment Directory Structure
-----------------------------
VoteApp
|
|------Java Resources
|		|
		|------src
|			|
			|----com.ihub.www
				|
				|---VoteSrv.java
|
|
|------Web content 
		|
		|-----form.html
|		|
		|------WEB-INF
			|
			|------web.xml
|

Note:
-----
In above application we need to add "servlet-api.jar" file in project build path.


We can send the request to servlet program in two methodologies.

1)GET methodology
--------------	
	It will carry limited amount of data.


2)POST methodology 
------------------
	It will carry unlimited amount of data.

While working with HttpServlet class,It is not recommanded to use service(-,-) method because they have not designed according to HTTP protocol.

It is always recommanded to use doXxx(-,-) method because they have designed according to HTTP protocol.

We have following seven doXxx(-,-) methods.

ex:
	doGet(-,-)
	doPost(-,-)
	doHead(-,-)
	doPut(-,-)
	doTrace(-,-)
	doDelete(-,-)
	doOptions(-,-)

prototype of doXxx(-,-)
--------------------------
protected void doGet(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
{
	
}

form.html
----------

<form action="test" method="GET">
	
	Name : <input type="text" name="t1"/> <br>
	Age : <input type="text" name="t2"/> <br>
	<input type="submit" value="vote"/>
</form>

web.xml
--------
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee" xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd" id="WebApp_ID" version="3.0">
 
 <servlet>
 	<servlet-name>VoteSrv</servlet-name>
 	<servlet-class>com.ihub.www.VoteSrv</servlet-class>
 </servlet>
 
 <servlet-mapping>
 	<servlet-name>VoteSrv</servlet-name>
 	<url-pattern>/test</url-pattern>
 </servlet-mapping>
 
 <welcome-file-list>
 	<welcome-file>form.html</welcome-file>
 </welcome-file-list>
 
</web-app>

TestSrv.java
------------
package com.ihub.www;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class VoteSrv extends HttpServlet 
{
	protected void doGet(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
	{
		PrintWriter pw=res.getWriter();
		res.setContentType("text/html");
		
		//reading form data
		String name=req.getParameter("t1");
		String sage=req.getParameter("t2");
		
		//converting string age to int age
		int age=Integer.parseInt(sage);
		
		if(age<18)
			pw.println("<center><font color='red'>"+name+" U r not eligible to vote</font></center>");
		else
			pw.println("<center><font color='green'>"+name+" U r eligible to vote</font></center>");
		pw.close();
	}
}

request url
------------
	http://localhost:2525/VoteApp/

























































