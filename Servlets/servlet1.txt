Q)What is web application?
==========================
Web application is a collection of web resource programs having the capability to 
generate web pages.

We have two types of web pages.

1)Static web pages  / Passive web pages
-------------------------------------
A web page with fixed content is called static web page.
ex:
	Home Page
	AboutUs page
	ContactUs page 
	Services page
	and etc.

2)Dynamic web pages / Active web pages
---------------------------------------
A webpage with dynamic content is called dynamic web page.

In dynamic web page our content will change based on request to request and time to time.

ex:
	Live cricket score page
	stock market share value page
	gmail inbox page and etc.


We have two types of web resource programs.

1)Static web resource program
---------------------------
It is responsible to generate static web pages.
ex:
	HTML program
	CSS program
	Bootstrap program
	Angular program
	and etc.
		
2)Dynamic web resource program 
-------------------------------
It is responsible to generate dynamic web pages.
ex:
	Servlet program
	JSP program 
	and etc.

Based on the position and execution web resource programs are categorized into two types.

1)Client Side web resource program
-----------------------------
A web resource program which executes at client side is called client side web resource 
program.
ex:
	HTML program,
	CSS program,
	Bootstrap program,
	angular program and etc.

2)Server Side web resource program 
------------------------------------
A web resource program which executes at server side is called server side web resource 
program.
ex:
	servlet program
	jsp program
	and etc.

Understanding the setup of web application and web resource program execution
==============================================================================
Diagram: servlet1.1

Java application will execute manually.

Web application and web resource programs are executed automatically at the 
time when they have requested.

with respect to the diagram
---------------------------
1) First we give the request to web resource program.

2) Web server will trap that request and passes that request to appropriate web resource program.

3)Web resource program will execute the logic to process the request.

4)Web resource program will communicate with database if necessary.

5)Web resource program gives the output to server.

6)Web server will send the output as dynamic web page.


What is deployment ?
--------------------
The process of keeping the web application in the server is called deployment.
Reverse is called undeployment.


Web server
===========
A webserver is a piece of software which is used to automate whole process of 
web application and web resource program execution.
ex:
	Tomcat , Resin,Glassfish, JBoss, Wildfly, Web logic   and etc.


Responsibilities of a web server
-----------------------------------
1) It takes continues request from client.

2) It traps that request and passes to appropriate web resource program.

3) It provides environment to deploy and undeploy the web application.

4) It allows client side web resource programs to execute at client side.

5) It add middleware services only to deployed web application.

6) It sends the output to browser window as dynamic response.

7) It automates whole process of web application and web resource program execution.


To execute java program we required JRE/JVM.

To execute Applets we required applet viewer or applet container.

To execute Servlet program we required servlet container.

To execute JSP program we required JSP container.


Web container
==============
It is a software application or program which is used to manage whole life cycle of 
web resource program i.e from birth to death.

Servlet container manages whole life cycle of servlet program.

Similarly , JSP container manages whole life cycle of jsp program.

Some part of industry considers servlet container and jsp container are web containers.



























