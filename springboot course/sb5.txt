Exception Handling in Spring Boot
==================================
If we give/pass wrong request to our application then we will get Exception.
ex:
	http://localhost:9090/books/102

Here '102' record is not available so immediately our controller will throw below exception.
ex:
{
    "timestamp": "2021-02-14T06:24:01.205+00:00",
    "status": 500,
    "error": "Internal Server Error",
    "path": "/books/102"
}	

Handling exceptions and errors in APIs and sending the proper response to the client is good for  enterprise applications.

In Spring Boot Exception handling can be performed  by using Controller Advice.

@ControllerAdvice
-------------------
The @ControllerAdvice is an annotation is used to to handle the exceptions globally.
 
@ExceptionHandler
--------------------
The @ExceptionHandler is an annotation used to handle the specific exceptions and sending the custom responses to the client.

Project structure
================
SBMySQLApp
|
|---src/main/java
|	|
|	|---com.ihub.www
|		|
|		|---SBMySQLAppApplication.java
|
|	|
|	|---com.ihub.www.controller
|		|	
|		|---BookController.java
|
|	|
|	|---com.ihub.www.model
|		|
|		|---Book.java
|
|
|	|
|	|---com.ihub.www.repo
|		|
|		|---BookRepository.java (Interface)
|
|
|	|
|	|---com.ihub.www.exception
|		|
|		|---ErrorDetails.java(POJO)
|		|---ResourceNotFoundException.java 
|		|---GlobalExceptionHandler.java
|
|
|---src/main/resources
|	|
|	|---application.properties
|
|---src/test/java
|	|
|	|---SBMySQLAppApplicationTests.java
|
|-
|-
|
|---pom.xml

step1:
------
	create a spring starter project.
	starter:  spring web
		  spring data jpa
		  Mysql Driver

step2:
------
	Create a Custom Exception class ResourceNotFoundException in com.ge.www.exception package.
ex:
package com.ihub.www.exception;

public class ResourceNotFoundException extends RuntimeException {
	public ResourceNotFoundException(String message)
	{
		super(message);
	}

}
	
step3:
-------
	To display custom exception details we can create ErrorDetails class.
exception details
-----------------
{
    "timestamp": "2021-02-14T07:55:09.466+00:00",
    "message": "ID NOT FOUND",
    "details": "uri=/books/333"
}	

ErroDetails.java
------------------
package com.ihub.www.exception;

import java.util.Date;

public class ErrorDetails {

	//take properties as per exception details(check above).
	private Date timestamp;
	private String message;
	private String details;
	
	
	
	public ErrorDetails(Date timestamp, String message, String details) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.details = details;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
}

step4:
---------
package com.ihub.www.exception;

public class ResourceNotFoundException extends RuntimeException 
{
	public ResourceNotFoundException(String message)
	{
		super(message);
	}

}
Note:
-----
	ResourceNotFoundException will handle specific exception not globally.

step5:
-------
	To Handle a Globally Exceptions we can create GlobalExceptionhandler class.

ex:
package com.ge.www.exception;

import java.ihub.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

	//handle specific exception
	
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<?> handleResourceNotFoundException
	(ResourceNotFoundException exception,WebRequest request )
	{
		ErrorDetails errorDetails=new ErrorDetails(new Date(),exception.getMessage(),request.getDescription(false));
		return new ResponseEntity<>(errorDetails,HttpStatus.NOT_FOUND);
	}
	
	//handle global exception
	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> handleException
	(Exception exception,WebRequest request )
	{
		ErrorDetails errorDetails=new ErrorDetails(new Date(),exception.getMessage(),request.getDescription(false));
		return new ResponseEntity<>(errorDetails,HttpStatus.INTERNAL_SERVER_ERROR);
	}
}

step6:
------
	create  a Book Model class.

ex:
package com.ihub.www.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="books")
public class Book {

		@Id
		private int bookId;
		
		@Column
		private String bookName;
		
		@Column
		private String authorName;
		
		public int getBookId() {
			return bookId;
		}
		public void setBookId(int bookId) {
			this.bookId = bookId;
		}
		public String getBookName() {
			return bookName;
		}
		public void setBookName(String bookName) {
			this.bookName = bookName;
		}
		public String getAuthorName() {
			return authorName;
		}
		public void setAuthorName(String authorName) {
			this.authorName = authorName;
		}
		@Override
		public String toString() {
			return "Book [bookId=" + bookId + ", bookName=" + bookName + ", authorName=" + authorName + "]";
		}
		
		
}
 
step7:
-------
	create a BookRepository interface by extending JpaRepository. 

ex:
package com.ihub.www.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ihub.www.model.Book;

public interface BookRepository extends JpaRepository<Book, Integer> {

}
	

step8:
--------
	create a BookController for add exceptions logic to handler methods. 

ex:
package com.ihub.www.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ge.www.exception.ResourceNotFoundException;
import com.ihub.www.model.Book;
import com.ihub.www.repo.BookRepository;


@RestController
public class BookController {
	
	@Autowired
	BookRepository bookRepository;
	
	@GetMapping("/books")
	public List<Book> getBooks()
	{
		return bookRepository.findAll();
	}
	
	//reading single
	@GetMapping("/books/{bookId}")
	public Book getBook(@PathVariable int bookId)
	{
		Book book=bookRepository.findById(bookId)
				.orElseThrow(()-> new ResourceNotFoundException("ID NOT FOUND"));
		
		return book;
	
	}
	
	//send
	@PostMapping("/books")
	public Book addBook(@RequestBody Book book)
	{
		bookRepository.save(book);
		return book; 
	}
	//update
	@PutMapping(path="/books/{bookId}",consumes = "application/json")
	public Book editBook(@RequestBody Book book, @PathVariable int bookId)
	{
		Book existingBook=bookRepository.findById(bookId)
				.orElseThrow(()->new ResourceNotFoundException("User Not found with ID"));
		System.out.println(existingBook);
		existingBook.setBookName(book.getBookName());
		existingBook.setAuthorName(book.getAuthorName());
		return bookRepository.save(existingBook); 
	}
	
	
	@DeleteMapping("/books/{bookId}")
	public Book deleteBook(@PathVariable int bookId)
	{
		Book book=bookRepository.findById(bookId)
				.orElseThrow(()->new ResourceNotFoundException("Id Not Found for Delete"));
		
		bookRepository.delete(book);

		return book;
	}
}


step9:
------
	add the mysql driver,hibernate and port no configuration in application.properties file.

ex:
server.port=9090

spring.datasource.driverClassName=com.mysql.cj.jdbc.Driver
spring.datasource.url= jdbc:mysql://localhost:3306/demo
spring.datasource.username=root
spring.datasource.password=root


#spring.jpa.hibernate.ddl-auto=create
spring.jpa.hibernate.ddl-auto=update
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5Dialect



step10:
-------
	Run the spring Boot application and test all the crud operations in
	POSTMAN as show in previous example.









RestTemplate
================
Spring RestTemplate class is part of spring-web , introduced in Spring 3. 

We can use RestTemplate to test HTTP based restful web services.

It doesn't support HTTPS protocol. 

RestTemplate class provides overloaded methods for different HTTP methods 
like GET, POST, PUT, DELETE  and etc

In short,RestTemplate is used to create applications that consume RESTful Web Services.


Project structure
================
SBMySQLApp
|
|---src/main/java
|	|
|	|---com.ihub.www
|		|
|		|---SBMySQLAppApplication.java
|		
|	|
|	|---com.ihub.www.controller
|		|	
|		|---BookController.java
|		|---RestClient.java
|	|
|	|---com.ihub.www.model
|		|
|		|---Book.java
|
|
|	|
|	|---com.ihub.www.repo
|		|
|		|---BookRepository.java (Interface)
|
|
|	|
|	|---com.ihub.www.exception
|		|
|		|---ErrorDetails.java(POJO)
|		|---ResourceNotFoundException.java 
|		|---GlobalExceptionHandler.java
|
|
|---src/main/resources
|	|
|	|---application.properties
|
|---src/test/java
|	|
|	|---SBMySQLAppApplicationTests.java
|
|-
|-
|
|---pom.xml

Note:
------
	Use existing project "SBMySQLApp" and perform following changes.


Book.java
-------------
package com.ihub.www.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="books")
public class Book {
	
	@Id
	@Column(name="bid")
	private int bookId;
	
	@Column(name="bname")
	private String bookName;
	
	@Column(name="bauthor")
	private String authorName;
	
	//default constructor
	public Book()
	{
		
	}
	
	//parameterized constructor
	public Book(int bookId, String bookName, String authorName) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.authorName = authorName;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", bookName=" + bookName + ", authorName=" + authorName + "]";
	}
	
	

}

RestClient.java
-------------------
package com.ihub.www.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.ihub.www.model.Book;

public class RestClient {
	
	public static final String GET_ALL_BOOKS = "http://localhost:9090/books";
	public static final String GET_BOOK_BY_ID = "http://localhost:9090/books/{bookId}";
	public static final String INSERT_BOOKS = "http://localhost:9090/books";
	public static final String UPDATE_BOOK_BY_ID = "http://localhost:9090/books";
	public static final String DELETE_BOOK_BY_ID = "http://localhost:9090/books/{bookId}";
	
	static RestTemplate restTemplate=new RestTemplate();
	
	public static void main(String[] args) {
		
		getAllBooks();
		//getBookById();
		insertBooks();
		updateBookById();
		deleteBookById();
		
	}
	public static void getAllBooks()
	{
		ResponseEntity<String> result=restTemplate.getForEntity(GET_ALL_BOOKS, String.class);
		System.out.println("====================");
		System.out.println(result);
		System.out.println("====================");
	}
	public static void getBookById()
	{
		Map<String,Integer> param=new HashMap<String,Integer>();
		param.put("bookId", 111);
		
		Book book=restTemplate.getForObject(GET_BOOK_BY_ID,Book.class,param);
		System.out.println("====================");
		System.out.println(book.getBookId());
		System.out.println(book.getBookName());
		System.out.println(book.getAuthorName());
		System.out.println("====================");
		
	}
	public static void insertBooks()
	{
		Book book=new Book(333,"Angular","James");
		ResponseEntity<Book> book2=restTemplate.postForEntity(INSERT_BOOKS, book, Book.class);
		System.out.println("=============");
		System.out.println(book2.getBody());
		System.out.println("=============");
	}
	
	
	public static void updateBookById()
	{
		/*
		Map<String,Integer> param=new HashMap<String,Integer>();
		param.put("bookId",101);
		Book updateBook=new Book(101,"ReactJS","Alan");
		restTemplate.put(UPDATE_BOOK_BY_ID, updateBook, param);
		*/

		Book book=new Book(102,"Spring","pro");
		restTemplate.put(UPDATE_BOOK, book,Book.class);
		System.out.println("please check the location");

	}
	
	
	public static void deleteBookById()
	{
		Map<String,Integer> param=new HashMap<String,Integer>();
		param.put("bookId",102);
		restTemplate.delete(DELETE_BOOK_BY_ID, param);
	}

}

Note:
---------
Run RestClient.java program as java application.
(Make sure spring boot application is also running).




Monolithic Architecture
=======================
Monolith means composed all in one piece.

The Monolithic application describes a single-tiered software application in which different
components combined into a single program from a single platform.

Diagram:sb5.1

In Monolithic Architecture we are developing every service individually and at end of thedevelopment we are packaging all services as single war file and deploying in a server.

Advantages
===========
1)Simple to develop
-----------------
At the beginning of a project it is much easier to go with Monolithic Architecture.

2)Simple to test
--------------
we can implement end-to-end testing by simply launching the application and testing the UI with Selenium.

3)Simple to deploy
---------------
 we have to copy the packaged application(war file) to a server.

4)Simple to scale
------------------
Simple to scale horizontally by running multiple copies behind a load balancer.


Drawbacks of Monolithic Architecture
==================================
1)Large and Complex Application
-------------------------------
It increase the size of the applications.

It become complex to understand and modify such applications.

As result development slows down and modularity breaks down over the time.

Moreever it is difficult to understand how to currently implement the change due to that quality of code will decline over the time.	

2)Slow Development
--------------------
As application and respective teams grows.The application really become difficult to understand and modify.

Due to large base code which leads to slower the IDE which makes programmers less productive.
	
3)Blocks Continenous development
------------------------------
In order to update one component/service.we need to re-deploy the entire application which interrupts the background.

There is also a chance ,the components which never have been updated failed to start correctly.As result risk associated with redeployment increases which discourage the continueous development.   		 	

4)Unscalable
-------------
We can't create instances for a perticular service.
we need to create instance for entire services present in a monolithic application.

5)Unreliable
---------------
Every service/component in monolethic application is tightly coupled.
If any one of the service/component goes down the entire system failed to run. 
	
Moreever,A bug in any component/service potentially bring down entire process.

6)Inflexible
------------
It is very difficult to adopt new frameworks and languages.
ex:
Microservices can't communicate each other.If they written in different languages. 		
	

MicroService Architecture
==============================
Microservices are the small services that work together

The microservice defines an approach to the architecture that divides an application into 
a pool of loosely coupled services that implements business requirements. 

In Microservice architecture,Each service is self contained and implements a single bussiness 
capability.

Diagram: sb5.2

The microservice architectural style is an approach to develop a single application as a suite of small services.It is next to Service-Oriented Architecture (SOA). 

Each microservice runs its process and communicates with lightweight mechanisms.

These services are built around business capabilities and independently developed by fully automated deployment machinery.


Advantages of Microservice Architecture
======================================
1)Independent Development
------------------------
	Each microservice can be developed independently.
	A single development team can build test and deploy the service.
	
2)Independent Deployment
------------------------
	we can update the service without redeploying the entire application.
	Bug release is more managable and less risky.

3)Fault Tolerance
------------------
	If service goes down ,It won't take entire application down with it.

4)Mixed Technology Stack
---------------------
	It is used to pick best technology which best suitable for our application.	

5)Granular Scaling
-----------------
	In Granular scaling ,services can scaled independently.Instead of entire application.



List of companies which are working with Microservices
====================================================
Amazon
NetFlix
SoundCloud
Twitter
Uber
PayPal
ebay
GILT
theguardian
NORDSTROM
and etc.











