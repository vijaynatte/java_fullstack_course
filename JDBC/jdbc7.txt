JDBC Flexible Application
==========================
In JDBC, Connection object is a heavy weight object.

It is not recommanded to create Connection object in every jdbc application.

It is always recommanded to create a seperate class which returns JDBC Connection object.

DBConnection.java
-----------------

package com.ihub.www;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection 
{
	static Connection con=null;
	
	public static Connection getConnection()
	{
		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","admin");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return con;
	}
}

FlexibleApp.java
----------------
package com.ihub.www;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class FlexibleApp 
{
	public static void main(String[] args)
	{
		Statement st=null;
		ResultSet rs=null;
		String qry=null;
		Connection con=null;
		try
		{
			con=DBConnection.getConnection();
			st=con.createStatement();
			qry="select * from student";
			rs=st.executeQuery(qry);
			while(rs.next())
			{
				System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getString(3));
			}
			rs.close();
			st.close();
			con.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}


Working with properties file
=============================
In regular intervals,Our DBA will change username and password for security reason.

It is not recommanded to pass database properties directly to the application. 

It is always recommanded to read database properties from properties file.

A properties file contains key and value pair.

dbdetails.properties
--------------------
driver=oracle.jdbc.driver.OracleDriver
url=jdbc:oracle:thin:@localhost:1521:XE
username=system
password=admin 

PropertiesFile.java
--------------------
package com.ihub.www;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

public class PropertiesFileApp 
{
	public static void main(String[] args)throws Exception 
	{
		//locate a properties file
		FileInputStream fis=new FileInputStream("src/com/ihub/www/dbdetails.properties");

		//create a properties class
		Properties p=new Properties();
		
		//load the data from properties file to properties class
		p.load(fis);
		
		//read the data from properties class
		String s1=p.getProperty("driver");
		String s2=p.getProperty("url");
		String s3=p.getProperty("username");
		String s4=p.getProperty("password");
		
		Class.forName(s1);
		Connection con=DriverManager.getConnection(s2,s3,s4);
		Statement st=con.createStatement();
		String qry="select * from student";
		ResultSet rs=st.executeQuery(qry);
		while(rs.next())
		{
			System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getString(3));
		}
		rs.close();
		st.close();
		con.close();
	}

}

DatabaseMetaData
====================
DatabaseMetaData is an interface which is present in java.sql package.

DatabaseMetaData provides metadata of a database. 

DatabaseMetaData gives information about database product name, database product version, database driver name , database driver version, username and etc.

We can create DatabaseMetaData object by using getMetaData() method of a Connection object.
ex:
	DatabaseMetaData dbmd=con.getMetaData();


ex:

package com.ihub.www;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;

public class DBMDApp {

	public static void main(String[] args)throws Exception 
	{
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","admin");
		
		DatabaseMetaData dbmd=con.getMetaData();
		
		System.out.println(dbmd.getDatabaseProductName());
		System.out.println(dbmd.getDatabaseProductVersion());
		System.out.println(dbmd.getDriverName());
		System.out.println(dbmd.getDriverVersion());
		System.out.println(dbmd.getUserName());
		
		con.close();
	}

}


ResultSetMetaData
====================
ResultSetMetaData is an interface which is present in java.sql package.

ResultSetMetaData provides details about metadata of a table.

ResultSetMetaData gives information about number of columns,type of columns, datatype of a columns and etc.

We can create ResultSetMetaData object by using getMetaData() method of a ResultSet object.
ex:
	ResultSetMetaData rsmd=rs.getMetaData();

ex:
----
package com.ihub.www;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

public class DBMDApp {

	public static void main(String[] args)throws Exception 
	{
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","admin");
		Statement st=con.createStatement();
		String qry="select * from student";
		ResultSet rs=st.executeQuery(qry);
		ResultSetMetaData rsmd=rs.getMetaData();
		
		System.out.println(rsmd.getColumnCount());
		System.out.println(rsmd.getColumnTypeName(2));
		System.out.println(rsmd.getColumnLabel(1));
		
		st.close();
		rs.close();
		con.close();
	}

}


Thin-Client/Fat-Server application
===================================
Every JDBC application is a thin-client/fat-server application.

To create a thin-client/fat-server application we need to save persistence logic and business logic in a database software in the form of PL/SQL procedures and functions.

To deal with PL/SQL procedures and functions we need to use CallableStatement object.

Diagram: jdbc7.1

PL/SQL stored procedure
=======================
create or replace procedure 
		first_proc(A IN number,B IN number, C OUT number)
		is
		BEGIN
		C:=A+B;
		END;
		/

ex:
----
package com.ihub.www;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Types;

public class CallableStmtApp {

	public static void main(String[] args)throws Exception
	{
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","admin");
		
		CallableStatement cst=con.prepareCall("{CALL first_proc(?,?,?)}");
		
		
		//register out parametr 
		cst.registerOutParameter(3, Types.INTEGER);
		
		//set the values to IN paramter
		cst.setInt(1, 10);
		cst.setInt(2, 20);
		
		//execute the procedure
		cst.execute();
		
		//gather the result from out parameter
		int result=cst.getInt(3);
		System.out.println("Sum of two numbers is ="+result);
		
		cst.close();
		con.close();
	}

}













































































































































































