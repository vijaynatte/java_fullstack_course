JSP to Database communication
==============================
Deployment Directory Structure
--------------------------------
JspApp6
|
|----Java Resources
|
|
|----Web Content
	|
	|----form.html
	|
	|----process.jsp
	|
	|----WEB-INF
		|
		|------web.xml
		|
		|------lib
			|
			|----ojdbc14.jar


Note:
------
If above application we need to add "servlet-api.jar" and "ojdbc14.jar" file in project build path.

form.html
----------
<form action="process.jsp">
	No: <input type="text" name="t1"/> <br>
	Name: <input type="text" name="t2"/> <br>
	Address:<input type="text" name="t3"/> <br>
	<input type="submit" value="submit"/>
</form>

process.jsp
-------------
<%@page import="java.sql.*" buffer="8kb" language="java"%>

<%
	String sno=request.getParameter("t1");
	int no=Integer.parseInt(sno);
	
	String name=request.getParameter("t2");
	String add=request.getParameter("t3");
	
	Connection con=null;
	PreparedStatement ps=null;
	int result=0;
	String qry=null;
	try
	{
		Class.forName("oracle.jdbc.driver.OracleDriver");
		con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","admin");
		qry="insert into student values(?,?,?)";
		ps=con.prepareStatement(qry);
		//set the values
		ps.setInt(1,no);
		ps.setString(2,name);
		ps.setString(3,add);
		
		//execute
		result=ps.executeUpdate();
		if(result==0)
			out.println("No Record Inserted");
		else
			out.println("Record is Inserted");
	}
	catch(Exception e)
	{
		out.println(e);
	}
%>


web.xml
-------
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee" xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd" id="WebApp_ID" version="3.0">
 
 <welcome-file-list>
 	<welcome-file>form.html</welcome-file>
 </welcome-file-list>
 
</web-app>




Request url
----------
	http://localhost:2525/JspApp6/


Action tags
===============
Action tags are used to provide funtionality along with Servlet API features.

It contains only xml tags.

IT does not have any standard tags.

All action tags executed dynamically at runtime only.

We have two types of acting tags.

1)Standard Action tags

2)Custom Action tags


1)Standard Action tags
-----------------
Built-in tags are called standard action tags.
ex:
	<jsp:include> 
	<jsp:forward>
	<jsp:useBean>
	<jsp:setProperty>
	<jsp:getProperty>
	and etc.


Action include tag
-----------------
In action include tag, our output of source jsp program and destination jsp program 
goes to browser window as response.

Action include uses servlet API functionality called rd.include(req,res).

Here code will not add to source jsp program.only output will add to source jsp program.

syntax:
--------
	<jsp:include  page="value"/>



Deployment Directory Structure
--------------------------------
JspApp7
|
|----Java Resources
|
|
|----Web Content
	|
	|----A.jsp
	|
	|----B.jsp
	|
	|----WEB-INF
		|
		|------web.xml


Note:
------
If above application we need to add "servlet-api.jar" file in project build path.

A.jsp
-------

<b><i>Begining of A.jsp</i></b>
<br>
<jsp:include page="B.jsp"/>
<br>
<b><i>Ending of A.jsp</i></b>

B.jsp
-------
<b><i>This is B.jsp program</i></b>

web.xml
----------
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee" xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd" id="WebApp_ID" version="3.0">
  
  <welcome-file-list>
  	<welcome-file>A.jsp</welcome-file>
  </welcome-file-list>
  
</web-app>

request url
------------
	http://localhost:2525/JspApp7/


Action forward tag
--------------------
In this case, our request goes to source jsp program and source jsp program will forward the
request destination jsp program.Here output of source jsp program will be discarded and 
output of destination program will goes to browser window as dynamic response.

Action forward uses servlet api functionality called rd.forward(req,res).

syntax:
	<jsp:forward page="page_name"/>

Deployment Directory Structure
--------------------------------
JspApp7
|
|----Java Resources
|
|
|----Web Content
	|
	|----A.jsp
	|
	|----B.jsp
	|
	|----WEB-INF
		|
		|------web.xml


Note:
------
If above application we need to add "servlet-api.jar" file in project build path.

A.jsp
-------

<b><i>Begining of A.jsp</i></b>
<br>
<jsp:forward page="B.jsp"/>
<br>
<b><i>Ending of A.jsp</i></b>

B.jsp
-------
<b><i>This is B.jsp program</i></b>

web.xml
----------
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee" xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd" id="WebApp_ID" version="3.0">
  
  <welcome-file-list>
  	<welcome-file>A.jsp</welcome-file>
  </welcome-file-list>
  
</web-app>

request url
------------
	http://localhost:2525/JspApp7/



JSP to bean communication
==========================
JSP to bean communication is possible using three tags.

1)<jsp:useBean>
-----------------
	It is used to create and locate bean class object.

2)<jsp:setProperty>
-------------------
	It is used to call setter method and used to set the values to bean class object.

3)<jsp:getProperty>
--------------------
	It is used to call getter method and used to get the values from bean class object.


Note:
------
	Above tags are independent tags.

example1
------------
Deployment Directory Structure
--------------------------------
JspApp8
|
|----Java Resources
|	|
	|-----src
		|
		|---------com.ihub.www
				|
				|----CubeNumber.java  
|
|----Web Content
	|
	|----index.jsp
	|
	|----WEB-INF
		|
		|------web.xml
Note:
------
If above application we need to add "servlet-api.jar" file in project build path.

index.jsp
----------
<jsp:useBean id="cn" class="com.ihub.www.CubeNumber"></jsp:useBean>

<%= "Cube of a given number is ="+cn.cube(5)%>

CubeNumber.java
----------------
package com.ihub.www;

public class CubeNumber 
{
	public int cube(int n)
	{
		return n*n*n;
	}
}

web.xml
--------
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee" xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd" id="WebApp_ID" version="3.0">
  
  <welcome-file-list>
  	<welcome-file>index.jsp</welcome-file>
  </welcome-file-list>
  
</web-app>

request url
--------
	http://localhost:2525/JspApp8/


example2
---------

Deployment Directory Structure
--------------------------------
JspApp9
|
|----Java Resources
|	|
	|-----src
		|
		|---------com.ihub.www
				|
				|----User.java  
|
|----Web Content
	|
	|----form.html
	|
	|----process.jsp
	|
	|----WEB-INF
		|
		|------web.xml
Note:
------
If above application we need to add "servlet-api.jar" file in pr	oject build path.

form.html
---------

<form action="process.jsp">
	UserName: <input type="text" name="username"/> <br>
	Password: <input type="password" name="password"/> <br>
	Email: <input type="text" name="email"/><br>
	<input type="submit" value="submit"/>
</form>

process.jsp
------------


<jsp:useBean id="u" class="com.ihub.www.User"></jsp:useBean>

<jsp:setProperty property="*" name="u"/>

Records are <br>
UserName : <jsp:getProperty property="username" name="u"/> <br>
Password : <jsp:getProperty property="password" name="u"/> <br>
Email : <jsp:getProperty property="email" name="u"/> <br>

web.xml
-------
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee" xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd" id="WebApp_ID" version="3.0">
  
  <welcome-file-list>
  	<welcome-file>form.html</welcome-file>
  </welcome-file-list>
  
</web-app>

User.java
----------
package com.ihub.www;

public class User 
{
	private String username;
	private String password;
	private String email;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}

Request url
-----------
	http://localhost:2525/JspApp9/





Custom Tags in jsp
=====================
Tags which are created by the programmer based onthe aplication requirement are called custom tags.

To work with JSP custom tags we need to add "jsp-api.jar" file in project build-path and "lib" folder of a project.

Taglib directive is used to create custom tags in jsp. 

syntax
-----
	<%@taglib uri="urioftaglibdirectory" prefix="prefixtaglibrary" %>

ex:
Deployment Directory structure
==============================
CustomTags
|
|----------Java Resources
|		|
		|------src
			|	
			|-----CubeNumber.java
|
|----------Web Content
|		|
		|------index.jsp
		|
		|------WEB-INF
			|
			|-----web.xml
			|-----mytags.tld
			|-----lib
				|
				|---jsp-api.jar

Note:
	In above project we need to add "servlet-api.jar" file and 
	"jsp-api.jar" file in project build path.

	We need to copy and paste "jsp-api.jar" file in 
	"Webcontent/WEB-INF/lib" folder seperately.

index.jsp
---------
<%@taglib uri="WEB-INF/mytags.tld" prefix="m"%>

cube of a given number is : <m:cube  number="5"/>

mytags.tld
----------
<?xml version="1.0" encoding="ISO-8859-1" ?>  
<!DOCTYPE taglib  
        PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.2//EN"  
    "http://java.sun.com/j2ee/dtd/web-jsptaglibrary_1_2.dtd">  
<taglib>  
  <tlib-version>1.0</tlib-version>  
  <jsp-version>1.2</jsp-version>  
  <short-name>simple</short-name>  
  <uri>http://tomcat.apache.org/example-taglib</uri>  
  
	<tag>
		<name>cube</name>
		<tag-class>com.ge.www.CubeNumber</tag-class>
		<attribute>
			<name>number</name>
			<required>true</required>	
		</attribute>
	</tag>
	
</taglib> 

CubeNumber.java
---------------
package com.ge.www;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class CubeNumber extends TagSupport{
	
		private int number;
		
		//setter method
		public void setNumber(int number)
		{
			this.number=number;
		}
		public int doStartTag()throws JspException
		{
			JspWriter out=pageContext.getOut();
			try
			{
				out.println(number*number*number);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			return SKIP_BODY;
		}
}


web.xml
--------
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee" xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd" id="WebApp_ID" version="3.0">
  
  <welcome-file-list>
  	<welcome-file>index.jsp</welcome-file>
  </welcome-file-list>
  
</web-app>

Request url
-----------
	http://localhost:2525/CustomTags/







































































