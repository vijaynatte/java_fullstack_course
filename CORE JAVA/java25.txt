Interface
==========
Interface is a collection of zero or more abstract methods.

Abstract method is a incomplete method which ends with semicolon and does not have any
body.
ex:
	public abstract void  m1();

It is not possible to create object for interfaces.

To write the implementation of abstract methods of an interface we will use 
implementation class.

It is possible to create object for implementation class because it contains 
method with body.

By default every abstract method is a public and abstract.

Interface contains only constants i.e public static final.

We can declare interface as follow.

syntax:
	modifier interface <interface_name>
	{
		-
		- //abstract methods
		-
	}

ex:1
-----
interface A
{
	//abstract method
	public abstract void m1();
}
class  B implements A
{
	public  void m1()
	{
		System.out.println("M1-Method");
	}
}
class Test
{
	public static void main(String[] args)
	{
		B b=new B();
		b.m1();
	}
}


ex:2
---
interface A
{
	//abstract method
	public abstract void m1();
}
class  B implements A
{
	public  void m1()
	{
		System.out.println("M1-Method");
	}
}
class Test
{
	public static void main(String[] args)
	{
		A a=new B();
		a.m1();
	}
}

ex:3
-----
interface A
{
	//abstract method
	public abstract void m1();
}

class Test
{
	public static void main(String[] args)
	{
		A a=new A()
		{
			public void m1()
			{
				System.out.println("M1-Method");
			}
		};
		a.m1();
	}
}

If interface contains four methods then we need to override all four methods in 
implementation class.

ex:
---
interface A
{
	public abstract void show();
	public void display();
	abstract void view();
	void see();
}
class B implements A
{
	public void show()
	{
		System.out.println("show method");
	}
	public void display()
	{
		System.out.println("display method");
	}
	public void view()
	{
		System.out.println("view method");
	}
	public void see()
	{
		System.out.println("see method");
	}
}
class Test
{
	public static void main(String[] args)
	{
		A a=new B();
		a.show();
		a.display();
		a.view();
		a.see();
	}
}

In java, a class can't extends more then one class.
But interface can can extends more then one interface.

ex:

interface A
{
	void m1();
}
interface B
{
	void m2();
}
interface C extends A,B
{
	void m3();
}
class D implements C
{
	public void m1()
	{
		System.out.println("M1-Method");
	}
	public void m2()
	{
		System.out.println("M2-Method");
	}
	public void m3()
	{
		System.out.println("M3-Method");
	}

}
class Test
{
	public static void main(String[] args)
	{
		C c=new D();
		c.m1();
		c.m2();
		c.m3();
	}
}

In java , a class can implements more then one interface.

ex:

interface Mother
{
	float HT=5.8f;
	void height();
}
interface Father
{
	float HT=6.2f;
	void height();
}
class Child implements Father,Mother
{
	public void height()
	{
		float height=(Mother.HT+Father.HT)/2;
		System.out.println("Child Height is ="+height);
	}
}
class Test
{
	public static void main(String[] args)
	{
		Child c=new Child();
		c.height();
	}
}

Q)What is marker interface?

Empty interface is called marker interface.

Interface which does not have any methods and constants is called marker interface.

Marker interfaces are those interfaces, using which we can get some ability to do.

ex:
	Serializable 
	Cloneable 
	Remote 
	and etc.




Abstract classes
================
Abstract class is a collection of zero or more abstract methods and zero or more 
concrete methods.

A "abstract" keyword is applicable for methods and classes but not for variables.

It is not possible to create object for abstract class.

To write the implementation for abstract methods of an abstract class we will use 
sub classes.

By default every abstract method is a public and abstract.

Abstract class contains only instance variables.

syntax:
------------
		abstract class <class_name>
		{
			-
			- // abstract methods
			- // concrete methods 
			-
		}

ex:
----
abstract class Plan
{
	//instance variable 
	protected double rate;

	//abstract method
	public abstract void getRate();

	//concrete method
	public void calculateBillAmt(int units)
	{
		System.out.println("Total Units : "+units);
		System.out.println("Total Bill  : "+(rate*units));
	}
}
class DomesticPlan extends Plan
{
	public void getRate()
	{
		rate=2.5d;
	}
}
class CommercialPlan extends Plan
{
	public void getRate()
	{
		rate=5.0d;
	}
}
class Test
{
	public static void main(String[] args)
	{
		DomesticPlan dp=new DomesticPlan();
		dp.getRate();
		dp.calculateBillAmt(250);

		CommercialPlan cp=new CommercialPlan();
		cp.getRate();
		cp.calculateBillAmt(250);

	}
}

Q)Difference between interface and abstract class?

Interface				Abstract class
------------------			----------------
To declare interface we will use 	To declare abstract class we will use abstract
interface keyword.			keyword.

It is a collection of 			It is a collection of abstract methods and 
abstract methods , default methods	concrete methods.
and static methods.

multiple inheritence is possible 	Multiple inheritance is not possible through	
through interface.			abstract class.

We can't declare blocks in interface.	We can declare blocks in abstract class.

Constructors are not allowed.		Constructors are allowed.

It contains constants.			It contains instance variables.

If we want to write the implementation	If we want to write the implementation of
of abstract methods of an interface	abstract methods of a abstract class we will
we will use implementation class.	use sub classes.

If we know only specification then	If we know partial implementation then we need  
we need to use interface.		to use abstract class.


Q)What is the difference between pojo class and java bean class?

pojo class
----------
A class is said to be pojo class if it supports following properties.

1)All variables must be private.

2)All variables must and should have setter and getter methods.

bean class
-----------
A class is said to be java bean class if it supports following four properties.

1) A class must be public

2) A class must have atleast zero argument constructor

3) All variables should be private.

4) All variables should have setter and getter methods.


Note:
----
	Every java bean class is a pojo class but every pojo class 
	is not a java bean class.


Q)What is singleton class?

A class which allows us to create only one object is called singleton class.

If we call any method using a class and that method returns same class object is 
called singleton class.
ex:
	LocalTime time=LocalTime.now();
	
To create a singleton class a class must have private constructor.

ex:
class Singleton
{
	private static Singleton s=null; 

	private Singleton()
	{
		System.out.println("const");
	}


	public static  Singleton  getInstance()
	{
		if(s==null)
		{
			s=new Singleton();
		}

		return s;
	}
}
class Test
{
	public static void main(String[] args)
	{
		Singleton s=Singleton.getInstance();
		System.out.println(s.hashCode());
	}
}



			























































































































