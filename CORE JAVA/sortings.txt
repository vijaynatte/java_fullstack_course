Bubble sort
===========

comparision sorting method
--------------------------

steps in bubble sort to make array as sorted
---------------------------------------------

ex:
	iteration 1
	-----------
	3 1 5 4 2 swap 3 & 1

	1 3 5 4 2 swap 3 & 5

	1 3 4 5 2

	1 3 4 2 5

	iteration 2
	-----------

	1 3 4 2 5

	1 3 2 4 5

	iteration 3
	-----------
	1 3 2 4 5

	1 2 3 4 5 

every iteration it will swap if the number greater than the next number

Note: for every iteration the last number is greater value so that there is no need to check last value


example 1:
	import java.util.Arrays;
	public class Test
	{
		public static void main(String[] args)
		{
			int[] arr = {3,1,5,4,2};
			bubble(arr);
			System.out.println(Arrays.toString(arr));
				
		}

		public static void bubble(int[] arr)
		{
			// run the steps n-1 times
			for(int i = 0; i<arr.length;i++)
			{
				//for each step max item will come at last respective index
				for(int j = 1;j<arr.length-i;j++)
				{	//swap if item is smaller than the previous item
					if(arr[j] < arr[j-1])
					{
						int temp = arr[j];
						arr[j] = arr[j-1];
						arr[j-1] = temp;
					}
				}
			}
		}	
	}

example 2: 

import java.util.Arrays;
	public class Test
	{
		public static void main(String[] args)
		{
			int[] arr = {1,2,3,4,5};
			bubble(arr);
			System.out.println(Arrays.toString(arr));
				
		}

		public static void bubble(int[] arr)
		{

			boolean  swapped;
			// run the steps n-1 times
			for(int i = 0; i<arr.length;i++)
			{
				swapped = false;
				//for each step max item will come at last respective index
				for(int j = 1;j<arr.length-i;j++)
				{	//swap if item is smaller than the previous item
					if(arr[j] < arr[j-1])
					{
						int temp = arr[j];
						arr[j] = arr[j-1];
						arr[j-1] = temp;
						swapped = true;
					}
				}
				//if you did not swap for a particular value of i, it means the array is sorted hence stop the program
				if (!swapped)
				{
					break;
				}
			}
		}	
	}



2) Selection sort
-----------------
select an element and put it at it's correct index

	ex:
		0 1 2 3 4 -> index values
		4 5 1 2 3 -> values of the array

		select the largest element in the array and place it in the last index place means
		swapping 5 with 3 because 3 present in the last index of the array. 
		
		0 1 2 3 4 
		4 3 1 2 5
		lly

		0 1 2 3 4 
		2 3 1 4 5 -> 2nd largest element is 4 and it should be placed in the last 2nd index so swapping 2 and 4

		0 1 2 3 4
		2 1 3 4 5

		0 1 2 3 4 
		1 2 3 4 5

we can also do this by taking smallest elements


ex:-
	import java.util.Arrays;
	public class Test
	{
		public static void main(String[] args)
		{
			int[] arr = {3,1,5,4,2};
			selection(arr);
			System.out.println(Arrays.toString(arr));
				
		}

		public static void selection(int[] arr)
		{
			for (int i = 0;i<arr.length ;i++ )
			{
				//find the max item in the remaining array and swap with correct index
				int last = arr.length - i - 1;
				int maxIndex = getMaxIndex(arr,0,last);

				swap(arr,maxIndex,last);
			}
		}

		public static void swap(int[] arr, int first, int second)
		{
			int temp = arr[first];
			arr[first] = arr[second];
			arr[second] = temp;
		}

		public static int getMaxIndex(int[] arr,int start,int end)
		{
			int max = start;
			
			for (int i = start;i<=end ;i++ )
			{
				if (arr[max] < arr[i])
				{
					max = i;
				}
			}
	
			return max;
		}

	
	}



3) insertion sort
-----------------

ex:-
	int[] arr = {12,31,25,8,32,17};

	in the above first we need to consider the first to elements,here we have 12 and 31 which are in ascending order so there is no need to swap

	- now we need move to the next two elements i.e 31 and 25 here 25 < 31 so need to swap here.....after swap we need to check 25 with his previous element
	i.e 12 so compare 12 and 25 it is in ascending order so no need to swap....now the array will be
	
	int[] arr = {12,25,31,8,32,17};

	now we need to compare next two elements which are 31 and 8 here 8<31 so the elements has to swap......now the array will be
	
	int [] arr = {12,25,8,31,32,17};
	again we need to check 8 with his previous or front elements so....we need to check 25 and 8.....here 8<25 so swap required

	int[] arr = {12,8,25,31,32,17};
	similarly we need to check for 12 and 8.....here 8<12 so swapping required.....after swapping the array will be

	int[] arr = {8,12,25,31,32,17};

	we need to follow the same procedure until the elements are sorted or the array is sorted.

	int[] arr = {8,12,25,31,32,17};

	int[] arr = {8,12,25,31,17,32};

	int[] arr = {8,12,25,17,31,32};

	int[] arr = {8,12,17,25,31,32};

	

it is used to sort an array by breaking into parts 
ex:

	import java.util.Arrays;
	public class Test
	{
		public static void main(String[] args)
		{
			int[] arr = {3,1,5,4,2};
			insertion(arr);
			System.out.println(Arrays.toString(arr));
				
		}

		public static void insertion(int[] arr)
		{
			for (int i =0;i<arr.length-1 ;i++ )
			{
				for (int j=i+1;j>0 ;j-- )
				{
					if (arr[j] < arr[j-1])
					{
						swap(arr, j, j-1);
					}
					else
					{
						break;
					}
				}
			}
		}

		public static void swap(int[] arr, int first, int second)
		{
			int temp = arr[first];
			arr[first] = arr[second];
			arr[second] = temp;
		}

		public static int getMaxIndex(int[] arr,int start,int end)
		{
			int max = start;
			
			for (int i = start;i<=end ;i++ )
			{
				if (arr[max] < arr[i])
				{
					max = i;
				}
			}
	
			return max;
		}

	
	}


4) Cyclic sort
---------------
	

ex:

import java.util.Arrays;
class Test
{
	public static void main(String[] args)
	{
		int[] arr = {5,3,4,2,1};
		sort(arr);
		System.out.println(Arrays.toString(arr));
	}

	public static void sort(int[] arr)
	{
		int i = 0;
		while (i<arr.length)
		{
			int correct = arr[i] - 1;
			if (arr[i] != arr[correct])
			{
				swap(arr,i,correct);
			}
			else 
			{
				i++;
			}
		}
	}

	public static void swap(int[] arr,int first,int second)
	{
		int temp = arr[first];
		arr[first] = arr[second];
		arr[second] = temp;
	}
}

Q) Missing Number ? 

input :
		nums = [3,0,1]
output : 
		2


ex:

import java.util.Arrays;
class Test
{
	public static void main(String[] args)
	{
		int[] arr = {3,0,1};
		int result= missingNumber(arr);
		System.out.println(result);
	}

	public static int missingNumber(int[] arr)
	{
		int i = 0;
		while (i<arr.length)
		{
			int correct = arr[i];
			if (arr[i] < arr.length && arr[i] != arr[correct])
			{
				swap(arr,i,correct);
			}
			else 
			{
				i++;
			}
		}

		//search for first missing number
		for (int index = 0;index<arr.length ;index++ )
		{
			if (arr[index] != index)
			{
				return index;
			}
		}
		//case 2
		return arr.length;
	}

	public static void swap(int[] arr,int first,int second)
	{
		int temp = arr[first];
		arr[first] = arr[second];
		arr[second] = temp;
	}
}

Q) Find all missing/disappeard elements in an array?

input:
		nums = {4,3,2,7,8,2,3,1}

output:
		[5,6]


import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
class Test
{
	public static void main(String[] args)
	{
		int[] arr = {4,3,2,7,8,2,3,1};
		System.out.println(findDisappearedNumbers(arr));
	}

	public static List<Integer> findDisappearedNumbers(int[] arr)
	{
		int i = 0;
		while (i<arr.length)
		{
			int correct = arr[i] - 1;
			if (arr[i] != arr[correct])
			{
				swap(arr,i,correct);
			}
			else 
			{
				i++;
			}
		}

		//find missing numbers
		List<Integer> ans = new ArrayList<>();

		for (int index = 0;index < arr.length ;index++ )
		{
			if (arr[index] != index+1)
			{
				ans.add(index+1);
			}
		}
		return ans;
	}

	

	public static void swap(int[] arr,int first,int second)
	{
		int temp = arr[first];
		arr[first] = arr[second];
		arr[second] = temp;
	}
}


Q) Find the duplicate number ? 

inputs:
		arr = {1,3,4,2,2}
ouput:
		2



import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
class Test
{
	public static void main(String[] args)
	{
		int[] arr = {1,3,4,2,4};
		System.out.println(findDuplicate(arr));
	}

	public static int findDuplicate(int[] arr)
	{
		
		int i = 0;

		while (i<arr.length)
		{
			if (arr[i]!=i+1)
			{
				int correct = arr[i] - 1;
				if (arr[i] != arr[correct])
				{
					swap(arr,i,correct);
				}
				else 
				{
					return arr[i];
				}
			}
			else
			{
				i++;
			}
		}
		return -1;
	}

	

	public static void swap(int[] arr,int first,int second)
	{
		int temp = arr[first];
		arr[first] = arr[second];
		arr[second] = temp;
	}
}


Q) Find all Duplicates in an array?

inputs:
		arr = [4,3,2,7,8,2,3,1]

output:
		[2,3]


import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
class Test
{
	public static void main(String[] args)
	{
		int[] arr = {4,3,2,7,8,2,3,1,8};
		System.out.println(findAllDuplicate(arr));
	}

	public static List<Integer> findAllDuplicate(int[] arr)
	{
		
		int i = 0;

		while (i<arr.length)
		{
			int correct = arr[i] - 1;
			if (arr[i] != arr[correct])
			{
				swap(arr,i,correct);
			}	
			else
			{
				i++;
			}
		}

		List<Integer> ans = new ArrayList<>();

		for (int index = 0;index < arr.length ;index++ )
		{
			if (arr[index] != index+1)
			{
				ans.add(arr[index]);
			}
		}

		return ans;
	}
	public static void swap(int[] arr,int first,int second)
	{
		int temp = arr[first];
		arr[first] = arr[second];
		arr[second] = temp;
	}
}

Q) Set Mismatch?

input:
	nums = [1,2,2,4];

output:
	[2,3]

or 

input:
	nums = [1,1];

output:
	[1,2]



import java.util.Arrays;

class Test {
    public static void main(String[] args) {
        int[] arr = {1, 2, 2, 4};
        System.out.println(Arrays.toString(findErrorNums(arr)));
    }

    public static int[] findErrorNums(int[] arr) {
        int i = 0;

        while (i < arr.length) {
            int correct = arr[i] - 1;
            if (arr[i] != arr[correct]) {
                swap(arr, i, correct);
            } else {
                i++;
            }
        }

        // Search for the first missing number
        for (int index = 0; index < arr.length; index++) {
            if (arr[index] != index + 1) {
                return new int[]{arr[index], index + 1};
            }
        }

        // If no error is found, return {-1, -1}
        return new int[]{-1, -1};
    }

    public static void swap(int[] arr, int first, int second) {
        int temp = arr[first];
        arr[first] = arr[second];
        arr[second] = temp;
    }
}


Q) First Missing Positive ? 

inputs: 
	arr = {1,2,0};

output:
	3

or

inputs: 
	arr = {3,4,-1,1};

output:
	2

or 

inputs: 
	arr = {7,8,9,11,12};

output:
	1


import java.util.Arrays;

class Test {
    public static void main(String[] args) {
        int[] arr = {1,2,0};
        System.out.println(firstMissingPositive(arr));
    }

   public static int firstMissingPositive(int[] arr)
	{
		int i = 0;
		while (i<arr.length)
		{
			int correct = arr[i] - 1;
			if (arr[i] > 0 && arr[i] <= arr.length && arr[i] != arr[correct])
			{
				swap(arr,i,correct);
			}
			else 
			{
				i++;
			}
		}

		//search for first missing number
		for (int index = 0;index<arr.length ;index++ )
		{
			if (arr[index] != index+1)
			{
				return index + 1;
			}
		}
		//case 2
		return arr.length+1;
	}

	public static void swap(int[] arr,int first,int second)
	{
		int temp = arr[first];
		arr[first] = arr[second];
		arr[second] = temp;
	}
}



















