Q) Find the floor of a number ?

class Test
{
	public static void main(String[] args)
	{
		int[] arr = {-7,-4,-2,0,2,3,5,9,11,16,22,28,29,60};
		int target = 2;

		int result = floorValue(arr,target);
		System.out.println(result);
	}

	public static int floorValue(int[] arr,int target)
	{
		int start = 0;
		int end = arr.length-1;

		while(start<=end)
		{
			int mid = start + (end - start)/2;
				
			if(target < arr[mid])
			{
				end = mid - 1;
			}
			else if (target > arr[mid])
			{
				start = mid + 1;
			}
			else 
			{
				return mid;
			}
		}
		return end;
	}
}


Q) Find the ceiling of a number ?

class Test
{
	public static void main(String[] args)
	{
		
		int[] arr = {-7,-4,-2,0,2,3,5,9,11,16,22,28,29,60};
		int target = 2;

		int result = floorValue(arr,target);
		System.out.println(result);
	}

	public static int ceilingValue(int[] arr,int target)
	{

		
		if(target > arr[arr.length-1])
		{
			return -1;
		}		

		int start = 0;
		int end = arr.length-1;

		while(start<=end)
		{
			int mid = start + (end - start)/2;
				
			if(target < arr[mid])
			{
				end = mid - 1;
			}
			else if (target > arr[mid])
			{
				start = mid + 1;
			}
			else 
			{
				return mid;
			}
		}
		return start;
	}
}

Q) find smallest letter greater than target?

class Test
{
	public static void main(String[] args)
	{
		char[] letters = {"c","f","j"};
		char target = "c";

		char result = nextGreateshChar(letters,target);

		System.out.println("the greatest char is : "+result);
	}

	public static char nextGreateshChar(char[] letters, char target)
	{
		int start = 0;
		int end = letters.length-1;

		while(start<=end)
		{
			int mid = start+(end-start)/2;
			
			if(target < letters[mid])
			{
				end = mid-1;
			}
			else
			{
				start = mid+1;
			}
		}
		return letters[start % letters.length];
	}
}


Q) Find first and last position of element in sorted array?

input :
	nums = {5,7,7,8,8,8,10} target = 8;

output : 
	[3,5]

if target not found then [-1,-1]



import java.util.Arrays;
class Test
{
	public static void main(String[] args)
	{
		int[] nums = {5,7,7,8,8,8,10};
		int target = 8;

		
		int[] result = searchRange(nums,target);
		System.out.println(Arrays.toString(result));
	}
	
	public static int[] searchRange(int[] nums,int target)
	{
		int[] ans = {-1,-1};		
		
		int start = search(nums,target,true);
		int end = search(nums,target,false);

		ans[0] = start;
		ans[1] = end;

		return ans;
	}

	public static int search(int[] nums,int target,boolean findFirstIndex)
	{
		int ans = -1;
		int start = 0;
		int end = nums.length-1;

		while(start<=end)
		{
			int mid = start + (end-start)/2;

			if(target < nums[mid])
			{
				end = mid - 1;
			}
			else if(target > nums[mid])
			{
				start = mid + 1;
			}
			else
			{      //potential answer found
				ans = mid;
				if (findFirstIndex)
				{
					end = mid - 1;
				}
				else
				{
					start = mid + 1;
				}
			}	
		}
		return ans;
	}
}


Q) Find position of an element in a sorted array of infinite numbers?

class Test
{
	public static void main(String[] args)
	{
		int[] arr = {3,5,7,9,10,90,100,130,140,160,170};
		int target = 10;
		System.out.println(ans(arr,target));
	}

	public static int ans(int[] arr,int target)
	{
		

		int start = 0;
		int end = 1;

		
		while(target > arr[end])
		{
			int newStart = end + 1;
			//double the box value
			// end = previous end + sizeOfBox * 2
		
			end = end + (end - start + 1) * 2; 
			start = newStart;
		}

		return binarySearch( arr, target, start, end);
	}

	public static int binarySearch(int[] arr,int target,int start,int end)
	{
		while(start<=end)
		{
			int mid = start + (end-start)/2;

			if(target < arr[mid])
			{
				end = mid -1;
			}
			else  if(target > arr[mid])
			{
				start = mid + 1;
			}
			else
			{
				return mid;
			}
		}
		return -1;
	}

}


Q) Peak index in a mountain array?

import java.util.Arrays;
class Test
{
	public static void main(String[] args)
	{
		int[] arr = {2,3,4,5,6,7,8,4,3,2};
		int result = findIndexInMountainArray(arr);
		System.out.println(result);
	}

	public static int findIndexInMountainArray(int[] arr)
	{
		int start = 0;
		int end = arr.length-1;

		while(start<end)
		{
			int mid = start + (end-start)/2;
			if(arr[mid] > arr[mid+1])
			{
				end = mid;
			}else{
				start = mid+1;
			}
				
		}
		return start;
	}
	
}


Q) Find in mountain array?

{bitonic array or mountain array}
import java.util.Arrays;
class Test
{
	public static void main(String[] args)
	{
		int[] arr = {1,2,3,4,5,3,1};
		int target = 4;
		int result = search(arr,target);
		System.out.println(result);
	}
	public static int search(int[] arr,int target)
	{
		int peak = peakIndexInMountainArray(arr);
		int firstTry = orderAgnosticBs(arr,target,0,peak);
		
		if(firstTry !=0)
		{
			return firstTry;
		}
		
		return orderAgnosticBs(arr,target,peak+1,arr.length-1);
	} 

	public static int peakIndexInMountainArray(int[] arr)
	{
		int start = 0;
		int end = arr.length-1;

		while(start<end)
		{
			int mid = start + (end-start)/2;
		
			if(arr[mid] > arr[mid+1])
			{
				end = mid;
			}
			else
			{
				start = mid+1;
			}
		}
		return start;
	}	

	public static int orderAgnosticBs(int[] arr,int target,int start,int end)
	{
		
		boolean isAsc = arr[start] < arr[end];


		while(start <= end)
		{

			int mid = start + (end-start)/2;

			if(arr[mid]== target)
			{
				return mid;
			}

			if(isAsc)
			{
				if(target < arr[mid])
				{
					end = mid - 1;
				}
				else
				{
					start = mid + 1;
				}
			}
			else	
			{
				if(target > arr[mid])
				{
					end = mid - 1;
				}
				else
				{
					start = mid + 1;
				}
			}
		}
		return -1;
	}
	
}


Q) Search in rotated sorted array?

pivot element is largest element in the sorted array 

//values are distinct/unique in the given array
input :
	 nums = {4,5,6,7,0,1,2} target = 0;
output :
	 4

input :
	 nums = {4,5,6,7,0,1,2} target = 3;
output :
	-1

input :
	 nums = {1} target = 0;
output :
	-1

//this logic will be easy by using recursion


ex:1
	public class Test
{
	public static void main(String[] args)
	{
		int[] arr = {4,5,6,7,0,1,2};
		System.out.println(findPivot(arr));
	}

	public static int search(int[] nums, int target)
	{
		int pivot = findPivot(nums);
		
		
		if (pivot == -1)
		{
			//then just do normal binary search 
			return binarySearch(nums,target,0,nums.length-1);
		}
		
		if (nums[pivot] == target)
		{
			return pivot;
		}

		if (target >= nums[0])
		{
			return binarySearch(nums,target,0,pivot-1);
		}
		return binarySearch(nums,target,pivot+1,nums.length-1);
	}

	
	public static int binarySearch(int[] arr,int target,int start,int end)
	{
		//int start = 0;
		//int end = arr.length-1;

		while(start<=end)
		{
			int mid = start + (end - start)/2;
			
			if (target < arr[mid])
			{
				end = mid - 1;
			}
			else if (target > arr[mid])
			{
				start = mid + 1;
			}
			else
			{
				return mid;
			}
		}
		return -1;
	}

	static int findPivot(int[] arr)
	{
		int start = 0;
		int end = arr.length-1;

		while (start<=end)
		{
			int mid = start + (end-start)/2;
			{
				return mid;
			}
			if (mid > start && arr[mid] < arr[mid+1])
			{
				return mid-1;
			}
			if ( arr[mid] <= arr[start])
			{
				end = mid-1;
			}
			else
			{
				start = mid + 1;
			}
		}
		return -1;
	}
}

ex:2

public class Test
{
	public static void main(String[] args)
	{
		int[] arr = {4,5,6,7,0,1,2};
		int target = 1;
		System.out.println(search(arr,target));
	}

	public static int search(int[] nums, int target)
	{
		int pivot = findPivot(nums);
		
		if (pivot == -1)
		{
			
			return binarySearch(nums,target,0,nums.length-1);
		}
		if (nums[pivot] == target)
		{
			return pivot;
		}

		if (target >= nums[0])
		{
			return binarySearch(nums,target,0,pivot-1);
		}
		return binarySearch(nums,target,pivot+1,nums.length-1);
	}

	public static int binarySearch(int[] arr,int target,int start,int end)
	{
	

		while(start<=end)
		{
			int mid = start + (end - start)/2;
			
			if (target < arr[mid])
			{
				end = mid - 1;
			}
			else if (target > arr[mid])
			{
				start = mid + 1;
			}
			else
			{
				return mid;
			}
		}
		return -1;
	}

	static int findPivot(int[] arr)
	{
		int start = 0;
		int end = arr.length-1;

		while (start<=end)
		{
			int mid = start + (end-start)/2;
			{
				return mid;
			}
			if (mid > start && arr[mid] < arr[mid+1])
			{
				return mid-1;
			}
			if ( arr[mid] <= arr[start])
			{
				end = mid-1;
			}
			else
			{
				start = mid + 1;
			}
		}
		return -1;
	}
}


note: for the above two logics won't work for dupicates.

ex:- below logic is for duplicates
public class Test
{
	public static void main(String[] args)
	{
		int[] arr = {4,5,6,7,0,1,2};
		int target = 1;
		System.out.println(search(arr,target));
	}

	public static int search(int[] nums, int target)
	{
		int pivot = findPivot(nums);
		
		if (pivot == -1)
		{
			
			return binarySearch(nums,target,0,nums.length-1);
		}
		if (nums[pivot] == target)
		{
			return pivot;
		}

		if (target >= nums[0])
		{
			return binarySearch(nums,target,0,pivot-1);
		}
		return binarySearch(nums,target,pivot+1,nums.length-1);
	}


	public static int binarySearch(int[] arr,int target,int start,int end)
	{
		//int start = 0;
		//int end = arr.length-1;

		while(start<=end)
		{
			int mid = start + (end - start)/2;
			
			if (target < arr[mid])
			{
				end = mid - 1;
			}
			else if (target > arr[mid])
			{
				start = mid + 1;
			}
			else
			{
				return mid;
			}
		}
		return -1;
	}

	static int findPivot(int[] arr)
	{
		int start = 0;
		int end = arr.length-1;

		while (start<=end)
		{
			int mid = start + (end-start)/2;


			if (mid < end && arr[mid] > arr[mid+1]) 			
			{
				return mid;
			}
			if (mid > start && arr[mid] < arr[mid+1])
			{
				return mid-1;
			}

			if (arr[mid] == arr[start] && arr[mid] == arr[end])
			{

				if (arr[start] > arr[start+1])
				{
					return start;
				}
				start++;
				
				//check if end is pivot
				if (arr[end] < arr[end - 1])
				{
					return end-1;
				}
				end--;
			}

			else if (arr[start] < arr[mid] || (arr[start] == arr[mid] && arr[mid] > arr[end]))
			{
				start = mid + 1;
			}
			else{
				end = mid - 1;
			}
				
		}
		return -1;
	}
}




Q) Find the Rotation count in rotated sorted array?

input: 
	arr[] = {15,18,2,3,6,12};

output":
	2


Note: the rotation will be always pivot times which means when we find a pivot value then it should add pivot index+1 that will be the answer of rotation count
	ex:
		normal array 3 4 5 6 7 8 10 -> values
		             0 1 2 3 4 5 6 -> indexes

		after rotation
				6 7 8 10 3 4 5  -> values here 4 times the array is rotated based on the greatest values means pivot value
				0 1 2  3 4 5 6 -> indexes -> here the value 10 lie in the index 3 but our index start from 0 so that we need to add 1 for the index to get exact rotation count



ex:-
	public class Test
	{
		public static void main(String[] args)
		{
			int[] arr = {4,5,67,0,1,2};
			System.out.println(countRotations(arr));
		}

		public static int countRotations(int[] arr)
		{
			int pivot = findPivot(arr);
			return pivot + 1;
		}

		public static int findPivot(int[] arr)
		{
			int start = 0;
			int end = arr.length-1;

			while (start<=end)
			{
				int mid = start + (end-start)/2;


				if (mid < end && arr[mid] > arr[mid+1]) 
				{
					return mid;
				}
				if (mid > start && arr[mid] < arr[mid-1])
				{
					return mid-1;
				}
				if ( arr[mid] <= arr[start])
				{
					end = mid-1;
				}
				else
				{
					start = mid + 1;
				}
			}
			return -1;
		}

	}


Q) Split Array Largest Sum?

input:
	nums = [7,2,5,10,8], m = 2;
output:
	18

or 

input:
	nums = [1,2,3,4,5] m = 2;
output:
	9

explanation
-------------
	There are Four wayst to split nums into two subarrays, the best way is to split it into [7,2,5] and [10,8] where the largest sum among the two subarrays is only 18.


class Test
{
	public static void main(String[] args)
	{
		int[] arr = {7,6,5,8,10,12};
		int m = 2;
		System.out.println(splitArray(arr,m));
	}
	public static int splitArray(int[] nums,int m)
	{
		int start = 0;
		int end = 0;

		for (int i = 0;i<nums.length-1 ;i++ )
		{
			start = Math.max(start,nums[i]);
			end = end + nums[i];
		}

		while (start < end)
		{

			int mid = start + (end-start)/2;

			int sum = 0;//initial sum
			int pieces = 1;//atleast one piece 

			for (int num : nums )
			{
				if (sum + num > mid)
				{

					sum = num;
					pieces++;
				}
				else 
				{
					sum += num;
				}
			}
			if (pieces > m)
			{
				start = mid+1;
			}else
			{
				end = mid;
			}
		}
		return start;//or end here start == end
	}
}










 

























