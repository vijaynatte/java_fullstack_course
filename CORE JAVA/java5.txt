Datatypes
=============
Datatype describes what type of value we want to store inside a variable.

Datatype also tells how much memory has to be created for a variable.

In java , we have two types of datatypes.

Diagram: java5.1
	
byte
-----
It is a smallest datatype in java.

Size: 1 byte (8 bits)

Range: -128 to 127 (-2^7 to 2^7-1)

ex:
	1) byte b=10;
	   System.out.println(b);//10

	2) byte b=130;
	   System.out.println(b);//C.T.E 

	3) byte b="hi";
	   System.out.println(b);//C.T.E 


short
--------
It is rarely used datatype in java.

Size: 2 bytes (16 bits)

Range : -32768 to 32767 (-2^15 to 2^15-1)

ex:
	1)short s="hello";
	  System.out.println(s);//C.T.E 

	2)short s=10.5;
	  System.out.println(s);//C.T.E 

	3)short s=true;
          System.out.println(s);//C.T.E 

int
------
It is mostly used datatype in java.

size: 4 bytes (32 bits)

Range: -2147483648 to 2147483647 (-2^31 to 2^31-1)

ex:
	1) int i="false";
	   System.out.println(i);//C.T.E

	2) int i=true;
	   System.out.println(i);//C.T.E 

	3) int i=10.5;
	   System.out.println(i);//C.T.E 

	4) int i='a';
	   System.out.println(i);//97 is a universal unicode value 

Note:
------
	a --> 97
	A --> 65

long
--------
If integer datatype is not enough to hold large number then we need to use long datatype.

Size: 8 bytes

Range : (-2^63 to 2^63-1)

ex:
	1) byte b=10;
	   short s=b;
	   int i=s;
	   long l=i;
	   System.out.println(l);//10

	2) long l=10.5;
	   System.out.println(l);//C.T.E 

	3) long l="hi";
	   System.out.println(l);//C.T.E 

	4) long l=true;
 	   System.out.println(l);//C.T.E 

	5) long l='A';
	   System.out.println(l);//65


float						double
---------					----------
If we need 4 to 6 decimal point of accuracy	If we need 14 to 16 decimal point of 
then we need to use float datatype.		accuracy then we need to use double 
						datatype.


Size: 4 bytes (32 bits)				Size: 8 bytes (64 bits)


Range: -3.4e38 to 3.4e38			Range: -1.7e308 to 1.7e308


To represent float value we need to 		To represent double value we need to 
suffix with 'f' or 'F'.				suffix with 'd' or 'D'.

ex:						ex:
	float f=10.5f;					double d=10.5d;
	

ex:
	1) float f=10.56f;
	   System.out.println(f);//10.56

	2) float f=10;
	   System.out.println(f);//10.0

	3) float f='a';
	   System.out.println(f);//C.T.E 

	4) float f="hi";
	   System.out.println(f);//C.T.E 

	5) float f=true;
	   System.out.println(f);//C.T.E 
	

ex:
	1) double d=10.56d;
	   System.out.println(d);//10.56

	2) double d=10;
	   System.out.println(d);//10.0

	3) double d='a';
	   System.out.println(d);//C.T.E 

	4) double d="hi";
	   System.out.println(d);//C.T.E 

	5) double d=true;
	   System.out.println(d);//C.T.E 


boolean 
-----------
It is used to represent boolean values either true or false.

Size:  (Not Applicable)

Range: (Not Applicable)

ex:
	1)boolean b="false";
	  System.out.println(b);//C.T.E 

	2)boolean b=TRUE;
	  System.out.println(b);//C.T.E cannot find symbol

	3) boolean b=true;
	   System.out.println(b);//true 


char
--------
It is a single character which is enclosed in a single quotation.

Size: 2 bytes (16 bits)

Range: 0 to 65535

ex:
	1) char c=10.5f;
	   System.out.println(c);//C.T.E 

	2) char c=100;
	   System.out.println(c);//d

	3) char c='a';
	   System.out.println(c);//a


Diagram : java5.2


ex:1
----
class Test
{
	public static void main(String[] args)
	{
		System.out.println(Byte.MIN_VALUE);	
		System.out.println(Byte.MAX_VALUE);
		
	}
}

ex:2
------
class Test
{
	public static void main(String[] args)
	{
		System.out.println(Integer.MIN_VALUE);	
		System.out.println(Integer.MAX_VALUE);
		
	}
}

Interview Question
====================
Q)What is operating system  , software , Application, Program, Instruction , Token ?

Operating System
--------------
It is a collection of softwares.
It is a mediator between software components and hardware components.

Software
-----------
It is a collection of Applications.

Application
-----------
It is a collection of programs.

Program
------
It is a collection of instructions.

Instruction
---------------
It is a collection of tokens.


Token
-----
It is a small unit of a program which consist of identifiers, keywords, constants,
datatypes,variables, operators and etc.

Diagram: java5.3


















































 

















































			