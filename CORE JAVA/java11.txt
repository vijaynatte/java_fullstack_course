Q)Can we execute java program without main method?

Yes , Untill java 1.6 version it is possible to execute java program without main method by 
using static block.But from java 1.7 version onwards it is not possible to execute java 
program without main method. 

ex:
class Test
{
	static
	{
		System.out.println("Hello World");
		System.exit(0);
	}
}	


ex:
------
class Test 
{
	public static void main(String[] args) 
	{
		int i=1,2,3,4,5;
		System.out.println(i);//C.T.E 
	}
}


ex:
----
class Test 
{
	int i=10;

	static int i=100;

	public static void main(String[] args) 
	{
		int i=1000;
		System.out.println(i);//C.T.E 
	}
}
o/p:variable i is already defined in class

ex:
-----
class Test
{
	public static void main(String[] args)
	{
		int i= 10 / 2;
		int j= 10 % 4;
		System.out.println(i+" "+j);//5   2
	}
}


ex:
-----
class Test
{
	public static void main(String[] args)
	{
		int i=10;
		i+=30;  // i = i + 30;
		System.out.println(i);//40
	}
}

ex:
-----
class Test
{
	public static void main(String[] args)
	{
		int i=10;
		i%=30;
		System.out.println(i);//10
	}
}


ex:
-----
class Test
{
	public static void main(String[] args)
	{
		int i=10;
		i/=30;
		System.out.println(i);//0
	}
}

ex:
----
class Test
{
	public static void main(String[] args)
	{
		int i=10;
		i*=3;
		System.out.println(i);//30
	}
}

Logical AND operator (&&)
-----------------------------
Logical AND operator deals with boolean values.

Truth table
------------
T	T	= T
T	F	= F
F	T	= F
F	F	= F

ex:
------
class Test
{
	public static void main(String[] args)
	{
		boolean b=(5>4) && (2<1);
		System.out.println(b);//false
	}
}

ex:
----
class Test
{
	public static void main(String[] args)
	{
		boolean b=true && (6>2);
		System.out.println(b);//true
	}
}


Logical OR operator (||)
-----------------------------
Logical OR operator deals with boolean values.

Truth table
-------------
T	T	= T
T	F	= T
F	T	= T
F	F	= F


ex:
------
class Test
{
	public static void main(String[] args)
	{
		boolean b=(2<-1) || (5>9);
		System.out.println(b);//false
	}
}

ex:
-----
class Test
{
	public static void main(String[] args)
	{
		boolean b=(2<9) || (5>9);
		System.out.println(b);//true
	}
}


Logical NOT operator (!)
----------------------------
class Test
{
	public static void main(String[] args)
	{
		boolean b= !(5>3);
		System.out.println(b);//false
	}
}


ex:
----
class Test
{
	public static void main(String[] args)
	{
		boolean b= !false;
		System.out.println(b);//true
	}
}

How to convert decimal to binary number
-----------------------------------------
	10   - decimal number 
	1010 - binary number 

	2|10
         ---- 0
        2|5
         ---- 1
        2|2
         ---- 0		^
	  1		|
	-----------------
	1010

How to convert binary to decimal number
----------------------------------------
	0101 - Binary number 
	5    - decimal number 

	
	0101
	    <----

	1*1 + 0*2 +  1*4  + 0*8
	1 + 0  + 4  + 0
	5



Bitwise AND operator (&)
----------------------------
Bitwise AND operator deals with binary numbers.

Truth table
-------------
T	T	= T
T	F	= F
F	T	= F
F	F	= F


ex:
-----
class Test
{
	public static void main(String[] args)
	{
		int a=10,b=15;
		int c= a & b;
		System.out.println(c); // 10
	}
}
/*
			10 - 1010
			15 - 1111
			-----------
			&  - 1010
						<---

		 0*1 + 1*2 + 0*4 + 1*8
		 0+2+0+8
		 10
*/

ex:
-----
class Test
{
	public static void main(String[] args)
	{
		int a=2,b=3;
		int c= a & b;
		System.out.println(c); //2
	}
}
/*
			2 - 0010
			3 - 0011
			---------
			& - 0010	

*/


Bitwise OR operator (|)
----------------------------
Bitwise OR operator deals with binary numbers.

Truth table
-----------
T	T 	= T
T	F	= T
F	T	= T
F	F	= F

ex:
----
class Test
{
	public static void main(String[] args)
	{
		int a=10,b=15;
		int c= a | b;
		System.out.println(c); //15
	}
}
/*
			10 - 1010
			15 - 1111
			-----------
			|  - 1111

*/

Bitwise XOR operator (^)
------------------------
Bitwise XOR operator deals with binary number.

Truth table
-----------
T	T	= F
T	F	= T
F	T	= T
F	F	= F

ex:
---
class Test
{
	public static void main(String[] args)
	{
		int a=10,b=5;
		int c= a ^ b;
		System.out.println(c); //15
	}
}
/*
		10 - 1010
		5  - 0101
		------------
		^  - 1111

*/

Arithmetic operators
--------------------
% - modules
/ - division
* - multiplication
+ - addition
- - subtraction

ex:
-----

class Test
{
	public static void main(String[] args)
	{
		int i= 5+6%3+8/6+9*4+3-6;
		System.out.println(i);
	}
}

/*
	5  +  6%3  +  8/6  +   9*4  +  3-6

	5  +  0  +  1  +  36   +  -3

	42 - 3

	39

*/


ex:
----
class Test
{
	public static void main(String[] args)
	{
		int i= 4*3+6%2+1;
		System.out.println(i);//
	}
}

/*
		4*3  +  6%2  +  1
		
		12 + 0 + 1

		13

*/



Ternary operator / conditional operator(?:) 
--------------------------------------------
syntax:
	(condition)?value1:value2;


ex:
---
class Test
{
	public static void main(String[] args)
	{
		boolean b=(5>3)?true:false;
		System.out.println(b);//true
	}
}


ex:
----
class Test
{
	public static void main(String[] args)
	{
		boolean b=(5>30)?true:false;
		System.out.println(b);//false
	}
}

Q)Write a java program to find out greatest of two numbers?

import java.util.Scanner;
class Test
{
	public static void main(String[] args)
	{
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the First Number :");
		int a=sc.nextInt();//10

		System.out.println("Enter the Second Number :");
		int b=sc.nextInt();//5

		int max=(a>b)?a:b;

		System.out.println("Greatest of two numbers is ="+max);
	}
}


Q)Write a java program to find out greatest of three numbers ? 


import java.util.Scanner;
class Test
{
	public static void main(String[] args)
	{
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the First Number :");
		int a=sc.nextInt();//10

		System.out.println("Enter the Second Number :");
		int b=sc.nextInt();//5

		System.out.println("Enter the Third Number :");
		int c=sc.nextInt();//15

		int max=(a>b)?(a>c?a:c):(b>c?b:c);		
                       
		System.out.println("Greatest of three numbers is ="+max);
	}
}


























































































































































































































