ii)Number Functions
-----------------------
ABS()
------
	It will return absoluate value.
	ex:
		select abs(-5) from dual;//5
		select abs(-10.45) from dual;//10.45


SQRT()
--------
	It will return exact square root value.
	ex:
		select sqrt(25) from dual; // 5
		select sqrt(81) from dual; //9
		select sqrt(26) from dual;//5.09

POWER(A,B)
------------
	It will return power value.
	ex:
		select power(2,3) from dual;//8
		select power(5,3) from dual;//125


CEIL()
---------
	It will return ceil value.
	ex:	
		select ceil(10.6) from dual;//11	
		select ceil(6.1) from dual;//7	

FLOOR()
---------
	It will return floor value.
	ex:
		select floor(10.7) from dual;//10
		select floor(4.2) from dual;//4

TRUNC()
---------
	It will remove decimal values.
	ex:
		select trunc(10.45) from dual;//10
		select trunc(98.12) from dual;//98

ROUND()
--------
	It will return nearest value.
	ex:
		select round(10.5) from dual;//11
		select round(10.4) from dual;//10

GREATEST()
------------
	It will return greatest value.
	ex:
		select greatest(10,2,67) from dual;//67
LEAST()
------
	It will return lowest value.
	ex:
		select least(10,2,67) from dual;//2


Working with Date values
==========================
Every database support different date patterns.

We need to insert date values in the pattern which is supported by database.
ex:
	Oracle --  dd-MMM-yy
	MySQL  --  yyyy-MM-dd


drop table emp1;

create table emp1(eid  number(3),ename varchar2(10),edoj date);

insert into emp1 values(501,'Alan','01-JAN-22');

insert into emp1 values(502,'Jose',sysdate);

insert into emp1 values(503,'Kelvin',current_date);

commit;

iii)Date functions
=====================
ADD_MONTHS()
---------------
	It will add months in a given date.
	ex:
		select ADD_MONTHS(sysdate,4) from dual;
		select ADD_MONTHS('01-JAN-22',3) from dual;
	
MONTHS_BETWEEN()
-------------------
	It will return number of months between two dates.
	ex:
		select MONTHS_BETWEEN('01-JAN-22','01-MAY-22') from dual;

NEXT_DAY()
-----------
	It will return next date of a given day in a week.
	ex:
		select NEXT_DAY(sysdate,'SUNDAY') from dual;		
		select NEXT_DAY(sysdate,'FRIDAY') from dual;

LAST_DAY()
----------
	It will return last date of a month.
	ex:
		select LAST_DAY(sysdate) from dual;
		select LAST_DAY('04-FEB-22') from dual;


iv)Convertion functions
=======================
Conversion function is used convert from one datatype to another datatype.

ex:
	TO_CHAR()

We have two pseudo for TO_CHAR().

1)number to_char()
----------------
It will accept '9' in digits and '$' symbol
ex:
	select eno,ename,esal from emp;
	select eno,ename,TO_CHAR(esal,'9,999') from emp;
	select eno,ename,TO_CHAR(esal,'99,999') from emp;
	select eno,ename,TO_CHAR(esal,'$99,999') from emp;

2)date to_char()
-------------------
ex:
	select sysdate from dual;		
	select TO_CHAR(sysdate,'dd-MM-yyyy') from dual;
	select TO_CHAR(sysdate,'dd-MM-yyyy') as DOJ from dual;
	select TO_CHAR(sysdate,'yyyy-MM-dd') as DOJ from dual;
	select TO_CHAR(sysdate,'DAY')  from dual;//Tuesday
	select TO_CHAR(sysdate,'DY')  from dual;//tue
	select TO_CHAR(sysdate,'Month')  from dual;//september
	select TO_CHAR(sysdate,'Mon')  from dual;//sep
	select TO_CHAR(sysdate,'year')  from dual;//twenty twenty two
	select TO_CHAR(sysdate,'yyyy')  from dual;//2022
	select TO_CHAR(sysdate,'hh:mi:ss')  from dual;
	select TO_CHAR(sysdate,'dd-MM-yyyy hh:mi:ss')  from dual;


group by clause
=================
It is used to divide the rows into groups so that we can apply group functions.

A column which we used in select clause , same column we need to use in group by clause.

Q)Write a query to display sum of salary of each department?

	select  sum(esal),deptno from emp group by deptno;



Q)Write a query to display highest salary of each job?

	select  max(esal),job from emp group by job;


Q)Write a query to display average salary of each department?

	select avg(esal),deptno  from emp group by deptno;


having clause
===============
It is used to filter the output from group by clause.


Q)Write a query to display sum of salary of each department where sum of salary is greater then 28000?

	select sum(esal),deptno from emp group by deptno having sum(esal)>28000;
	

Q)Write a query to display  minimum salary of each job where salary is less then 
	30000?

	select  min(esal),job from emp group by job having min(esal)<30000;


order by clause 
=================
Order by clause is used to arrange the rows in database table.

By default it will arrange the records in ascending order.

ex:
	select * from emp order by eid;

	select * from emp order by eid desc;

	select * from emp order by ename;
	
	select * from emp order by ename desc;
	

Q)Write a query to display sum of salary of each department except 10 department where sum of salary is greater then 50000 ?

	select  sum(esal),deptno from emp where deptno<>10 group by deptno
	having sum(esal)>50000 order by deptno;


























