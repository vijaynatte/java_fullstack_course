DCL commands
============
grant
revoke 

Schema
--------
Schema is a memory location which is used to run SQL commands.

Privileges
-----------
Permissions given to a user is called privileges.
or
Rights given to a user is called privileges.

We have two types of privileges.

1)System privilege

2)Object privilege

1)System privilege
-------------------
Permission given by DBA to user is called system privilege.

2)Object privilege
-----------------
Permission given by one user to another user is called object privilege.


grant command
--------------
It is used to grant the permissions.

syntax:
	grant <privilege1>,<privilege2> to <user_name>;

revoke command
-------------
It is used to revoke the permissions.

syntax:
	revoke <privilege1>,<privilege2> from <user_name>;


DBA> create user jyothi identified by jyothi;

DBA> create user pavan identified by pavan;

jyothi>  conn jyothi/jyothi    logon denied

pavan>  conn pavan/pavan    logon denied

DBA> grant connect,resource to jyothi,pavan;


pavan> 
create table employees(eid number(3),ename varchar2(10),esal number(10,2));

insert into employees values(501,'Alan',10000);
insert into employees  values(502,'Mark',20000);
insert into employees  values(503,'Jose',30000);

commit;

select * from employees ; // 3 records selected


jyothi> select * from employees; //table or view does not exist

pavan> grant select on employees to jyothi;

jyothi> select * from pavan.employees;

jyothi> delete from pavan.employees; // insufficient privileges

pavan> grant delete ,update on employees to jyothi;

jyothi> delete from pavan.employees;
jyothi> commit;

pavan> select * from employees; //no rows selected

pavan> revoke select,update,delete on employees from jyothi;

DBA>drop user jyothi cascade;
DBA>drop user pavan cascade; 

SEQUENCE
==========
SEQUENCE is an object which is used to generate the numbers.

syntax:
	create sequence <sequence_name> start with value increment by value;

ex:
	create sequence sq1 start with 1 increment by 1;
	create sequence sq2 start with 10 increment by 10;
	create sequence sq3 start with 101 increment by 1;

SEQUENCE contains two pseudo's.

1)NEXTVAL
--------
	It will generate next number in sequence.

2)CURRVAL
--------
	It will return the last number which is generated by sequence.

ex:
----
create sequence sq1 start with 101 increment by 1;

drop table student;
create table student(sno number(3),sname varchar2(10),sadd varchar2(10));

insert into student values(sq1.NEXTVAL,'raja','hyd');
insert into student values(sq1.NEXTVAL,'ravi','delhi');
insert into student values(sq1.NEXTVAL,'ramana','vizag');

commit;

select * from student;


select sq1.CURRVAL from dual; //103

Interview Queries
----------------
Q)Write a query to see list of sequences present in database?

	select sequence_name from user_sequences;


Q)Write a query to drop the sequence from database?




	drop sequence sq1;


Q)Write a query to read first three records from emp table?

	select * from emp where rownum<4;


Q)Write a query to read exact 4 record from emp table?

	select * from emp where rownum=4; //no rows selected

	select * from emp where rownum<=4
	minus
	select * from emp where rownum<=3;



























