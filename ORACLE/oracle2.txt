create command
=================
It is used to create a table in a database.
syntax:
	create table <table_name>(col1 datatype(size),col2 datatype(size),....,
				colN  datatype(size));

ex:
	create table student(sno number(3),
				sname varchar2(10),
				   sadd varchar2(12));

	
	create table dept(deptno number(3),
				dname varchar2(10),
					dloc varchar2(12));


	create table emp(eid number(3),ename varchar2(10),esal  number(10,2),
			deptno number(3),job varchar2(10),comm number(6));

describe command
================
Describe command is used to see the structure of a table.

syntax:
	desc  <table_name>;

ex:
	desc student;
	desc emp;
	desc dept;

insert command
=============
It is used to insert the records into a database table.
syntax:
	insert into <table_name> values(val1,val2,...,valN);

ex:
	insert into student values(101,raja,hyd); //invalid 

	insert into student values(101,'raja','hyd'); //valid 

	insert into student values('ravi',102,'delhi'); //invalid 

	insert into student values(102,'ravi'); //invalid 

null
-------
	null is a keyword which represent undefined or unavailable.
	ex:
	insert into student values(102,'ravi',null); //valid 	


approach2
----------
	insert into student(sno,sname,sadd) values(103,'ramana','vizag');

	insert into student(sno,sname) values(104,'raj');

approach3
---------
	Using '&' symbol we can read dynamic inputs.
	ex:
	insert into student values(&sno,'&sname','&sadd');


commit command
============
It is used to make the changes permanent to database.

syntax:
	commit;

emp table
===========
create table emp(eid number(3),ename varchar2(10),esal  number(10,2),
			deptno number(3),job varchar2(10),comm number(6));

insert into emp values(201,'Alan',9000,10,'Clerk',null);
insert into emp values(202,'Jose',19000,10,'Clerk',100);

insert into emp values(203,'Kelvin',26000,20,'HR',200);
insert into emp values(204,'Linda',42000,20,'HR',600);

insert into emp values(205,'Lisa',19000,30,'Manager',900);
insert into emp values(206,'James',29000,30,'Manager',800);
commit;


dept table
==========
create table dept(deptno number(3),
				dname varchar2(10),
					dloc varchar2(12));

insert into dept values(10,'ECE','HYD');
insert into dept values(20,'EEE','DELHI');
insert into dept values(30,'CSE','PUNE');
insert into dept values(40,'MEC','VIZAG');
commit;


select command
===============
It is used to select the records from database table.

syntax:
	select * from <table_name>;
	
	Here '*' means all rows and columns.
	
ex:
	select * from student;
	select * from emp;
	seelct * from dept;

Projection
===========
Selecting specific columns from database table is called projection.
ex:
	select * from student;
	select sno from student;
	select sname from student;
	select sno,sname from student;
	select sno,sname,sadd from student;

In select command we can perform arithmetic operations also.

ex:
	select  sno  from student;
	select  sno-100  from student;
	select  sno+100 from student;


column alias
================
A userdefined heading given to a column is called column alias.
colum alias is temperory.
Once the query is executed we will loss the column alias.
Column alias we can applied to any column.
ex:
	select sno+100 as SNO from student;	

	select sno as ROLLNO,
		 sname as FIRST_NAME,
			sadd as CITY 
				from student;

Interview Queries
==================
Q)Write a query to display all the table present in database?

	select * fromm tab;

Q)Write a query to display logical database name / schema ? 

	select * from global_name;  10g - XE  / 11g - ORCL 	

Q)Write a query to display all the users present in database?


	select * from all_users;

Q)Write a query to display all employees information from emp table?

	select * from emp;

Q)Write a query to display employee id , employee name and employee salary from emp table?

	select 	eid,ename,esal from emp;

Q)Write a query to display employee id ,employee name , employee salary and 
annual salary of each employee?

	select eid,ename,esal,esal*12 from emp; 


Q)Write a query to display employee id ,employee name , employee salary and 
annual salary as ANNUAL_SAL of each employee?

	select eid,ename,esal,esal*12  as ANNUAL_SAL from emp; 

where clause
===============
A where clause is used to retrieve the specific records from the database table.

syntax:
	select * from <Table_name> where condition;

ex:
	select * from student where sno=101;
	select * from student where sname='ramana';
	select * from student where sname='RAMANA'; // no rows selected
	select * from student where sadd='pune';	

Interview Queries
==================
Q)Write a query to display all employee information whose comm is null?

	select * from emp where comm = null; //no rows selected 

	select * from emp where comm is null;


Q)Write a query to display employees information of those who are working in 10 department?

	select * from emp where deptno=10;

	

Q)Write a query to display employee id , employee name and employee salary 
of employees those who are working as a manager?

	select eid,ename,esal from emp where job='Manager';
































































