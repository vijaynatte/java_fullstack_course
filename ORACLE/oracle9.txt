
select * from emp; //6 
select * from dept; //4
select * from emp,dept; // 24 records 

select eid,ename,esal,dname,dloc from emp,dept; //24 records 

select eid,ename,esal,deptno,dname,dloc from emp,dept;//column ambiguously defined

If two tables contain common column then we need to use tablename.columnname.
ex:
	select emp.eid,emp.ename,emp.esal,
			dept.deptno,dept.dname,dept.dloc from 
				emp,dept;// 24 records

table alias
------------
Table alias is temperory.

Using table alias length of the query will reduce and mean while performance will be maintained.

ex:
	 select e.eid,e.ename,e.esal,
			d.deptno,d.dname,d.dloc from 
				emp e,dept d;// 24 records


Joins
=========
Joins is used to retrieve the data from one or more then one table.

We have different types of joins.

1)Equi-Join

2)Non-Equi join

3)Self join

4)cartisian product

5)Inner join

6)Outer join 

1)Equi-Join
--------------
When two tables are joined based on common column is called equi join.

ex:
	select e.eid,e.ename,e.esal,d.dname,d.dloc 
	from emp e,dept d 
	where (e.deptno=d.deptno); // 6 records 

2)Non-Equi join
----------------
When two tables are not joined without using join condition is called non-equi join.

ex:
	select e.eid,e.ename,e.esal,d.dname,d.dloc from emp e,dept d
	where esal>25000;//3*4= 12 records 

3)Self join
-------------
When table is joined to itself is called self join.

In self join, we need to create two table alias for same table.

ex:
	select e1.eid,e1.ename,e1.esal,e2.job,e2.comm from emp e1,emp e2
	where(e1.deptno=e2.deptno); // 6 + 6 = 12 records 

4)cartisian product
--------------------
It will return all possible combinations.
ex:
	select e.eid,e.ename,e.esal,d.dname,d.dloc from emp e,dept d; 
							//6*4=24 records 

5)Inner join
----------------
It is similar to equi join.

ex:
	select e.eid,e.ename,e.esal,d.dname,d.dloc 
	from emp e INNER JOIN dept d 
	ON (e.deptno=d.deptno); // 6 records 

ex:
	select e.eid,e.ename,e.esal,d.dname,d.dloc 
	from emp e JOIN dept d 
	ON (e.deptno=d.deptno);


6)Outer join 
----------------
Outer join is a extension of equi join.

Outer join will return matching as well as not matching records.

A '+' will denoted as outer join operator.

We have following outer joins.

1)Left outer join

2)right outer join

3)full outer join 	

1)Left outer join
-----------------

	SQL
	----
		select e.eid,e.ename,e.esal,e.deptno,d.deptno,d.dname,d.dloc
		 from emp e,dept d
		where(e.deptno=d.deptno(+));
	ANSI 
	----
		select e.eid,e.ename,e.esal,e.deptno,d.deptno,d.dname,d.dloc
		 from emp e LEFT OUTER JOIN dept d
		ON(e.deptno=d.deptno);


2)right outer join
-------------------
	SQL
	----
		select e.eid,e.ename,e.esal,e.deptno,d.deptno,d.dname,d.dloc
		 from emp e,dept d
		where(e.deptno(+)=d.deptno);
	ANSI 
	----
		select e.eid,e.ename,e.esal,e.deptno,d.deptno,d.dname,d.dloc
		 from emp e RIGHT OUTER JOIN dept d
		ON(e.deptno=d.deptno);

3)full outer join 
----------------------
	ANSI
	-----
		select e.eid,e.ename,e.esal,e.deptno,d.deptno,d.dname,d.dloc
		 from emp e FULL OUTER JOIN dept d
		ON(e.deptno=d.deptno);

Synonyms
===========
Alter name given to a table is called synonym.

We can use synonym name instead of table name for the commands.

syntax:
	create synonym <synonym_name> for <object_name>;

ex:
	create synonym sy1 for student;

ex:
	select * from sy1;
	delete from sy1;// 3 rows deleted


Q)Write a query to see all the list of synonyms present in database?

	select synonym_name from user_synonyms;


Q)Write a query to drop the synonym from database?

	drop synonym sy1;





















